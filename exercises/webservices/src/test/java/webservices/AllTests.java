package webservices;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import webservices.bindingstyles.CarServiceClientTest;
import webservices.helloworld.HelloWorldClientTest;
import webservices.wsgen.GreetingWebServiceTest;
import webservices.wsimport.WsimportTest;

@RunWith(Suite.class)
@SuiteClasses({ CarServiceClientTest.class, HelloWorldClientTest.class, GreetingWebServiceTest.class,
		WsimportTest.class })
public class AllTests {

}
