package webservices.helloworld;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

/**
 * Implement the HelloWorld web service
 */
@WebService(endpointInterface = "webservices.helloworld.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

	public static void main(String[] args) {	
		// Implement so that this web service can start itself
		Endpoint.publish("http://localhost:8180/paul", new HelloWorldImpl());
	}

	public String helloWorld() {
		return "Hello world";
	}

}
