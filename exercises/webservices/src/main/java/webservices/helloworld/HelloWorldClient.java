package webservices.helloworld;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class HelloWorldClient {

	public static void main(String[] args) throws Exception {

		// Setup WS
		URL url = new URL("http://localhost:8180/paul?wsdl");
		QName qName = new QName("http://helloworld.webservices/", "HelloWorldImplService");
		Service service = Service.create(url, qName);
		HelloWorld port = service.getPort(HelloWorld.class);

		System.out.println("Response: " + port.helloWorld());

	}
}
