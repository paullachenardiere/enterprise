package webservices.helloworld;

import javax.jws.WebService;

/**
 * Add annotations to make this a web service
 */
@WebService
public interface HelloWorld {

	public String helloWorld();

}
