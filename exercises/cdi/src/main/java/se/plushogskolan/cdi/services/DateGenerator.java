package se.plushogskolan.cdi.services;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import org.joda.time.DateTime;

import se.plushogskolan.cdi.annotations.TodaysDate;
import se.plushogskolan.cdi.annotations.YesterdaysDate;

/**
 * A class for producing a dates, as specified by the @TodaysDate and @YesterdaysDate
 * annotations.
 * 
 */
@ApplicationScoped
public class DateGenerator {
	
	
	@Produces @Named @TodaysDate 
	public String getTodaysDate() {
		return new DateTime().toString("dd-MM-yyyy");
	}

	@Produces @Named @YesterdaysDate
	public String getYesterdaysDate() {
		DateTime date = new DateTime();
		date = date.minusDays(1);
		return date.toString("dd-MM-yyyy");
	}

}
