package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for LatitudeValidator.class 
 * @author PaulsMacbookPro
 */
public class LatitudeValidatorTest {

	LatitudeValidator validator;

	@Before
	public void init() {
		validator = new LatitudeValidator();
	}
	
	// Test String
	@Test
	public void testLatitudeValidatorStringTrue1() {
		assertTrue("true", validator.isValid("180", null));
	}
	@Test
	public void testLatitudeValidatorStringTrue2() {
		assertTrue("true", validator.isValid("-180", null));
	}
	@Test
	public void testLatitudeValidatorStringFalse1() {
		assertFalse("false", validator.isValid("0", null));
	}
	@Test
	public void testLatitudeValidatorStringFalse2() {
		assertFalse("false", validator.isValid("181", null));
	}
	@Test
	public void testLatitudeValidatorStringFalse3() {
		assertFalse("false", validator.isValid("-181", null));
	}
	
	
	// Test double
	@Test
	public void testLatitudeValidatorDoubleTrue1() {
		assertTrue("true", validator.isValid((double)180, null));
	}
	@Test
	public void testLatitudeValidatorDoubleTrue2() {
		assertTrue("true", validator.isValid((double)-180, null));
	}
	@Test
	public void testLatitudeValidatorDoubleFalse1() {
		assertFalse("false", validator.isValid((double)0, null));
	}
	@Test
	public void testLatitudeValidatorDoubleFalse2() {
		assertFalse("false", validator.isValid((double)181, null));
	}
	@Test
	public void testLatitudeValidatorDoubleFalse3() {
		assertFalse("false", validator.isValid((double)-181, null));
	}
	
	// NULL
	public void testLatitudeValidatorNull() {
		assertFalse("false", validator.isValid(null, null));
	}
}
