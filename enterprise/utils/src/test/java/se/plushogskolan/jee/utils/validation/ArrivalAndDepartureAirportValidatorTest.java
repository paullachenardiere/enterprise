package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for ArrivalAndDepartureAirportValidator.class 
 * @author PaulsMacbookPro
 */
public class ArrivalAndDepartureAirportValidatorTest {

	ArrivalAndDepartureAirportValidator validator;

	@Before
	public void init () {
		validator = new ArrivalAndDepartureAirportValidator();
	}

	@Test
	public void testNull() {

		assertFalse("Null ", validator.isValid(new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return null;
			}
			@Override
			public String getArrivalAirportCode() {
				return null;
			}
		}, null));
	}

	@Test
	public void testTrue() {

		assertTrue("True ", validator.isValid(new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return "ABC";
			}
			@Override
			public String getArrivalAirportCode() {
				return "CDE";
			}
		}, null));
	}

	@Test
	public void testSameCode() {

		assertFalse("Same come ", validator.isValid(new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return "ABC";
			}
			@Override
			public String getArrivalAirportCode() {
				return "ABC";
			}
		}, null));
	}

	@Test
	public void testOneNull() {

		assertFalse("OneNull ", validator.isValid(new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return null;
			}
			@Override
			public String getArrivalAirportCode() {
				return "ABC";
			}
		}, null));
	}
}
