package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for LongutudeValidator.class 
 * @author PaulsMacbookPro
 */
public class LongitudeValidatorTest {

	LongitudeValidator validator;

	@Before
	public void init() {
		validator = new LongitudeValidator();
	}

	// Test String
	@Test
	public void testLongitudeValidatorStringTrue1() {
		assertTrue("true", validator.isValid("180", null));
	}
	@Test
	public void testLongitudeValidatorStringTrue2() {
		assertTrue("true", validator.isValid("-180", null));
	}
	@Test
	public void testLongitudeValidatorStringFalse1() {
		assertFalse("false", validator.isValid("0", null));
	}
	@Test
	public void testLongitudeValidatorStringFalse2() {
		assertFalse("false", validator.isValid("181", null));
	}
	@Test
	public void testLongitudeValidatorStringFalse3() {
		assertFalse("false", validator.isValid("-181", null));
	}


	// Test double
	@Test
	public void testLongitudeValidatorDoubleTrue1() {
		assertTrue("true", validator.isValid((double)180, null));
	}
	@Test
	public void testLongitudeValidatorDoubleTrue2() {
		assertTrue("true", validator.isValid((double)-180, null));
	}
	@Test
	public void testLongitudeValidatorDoubleFalse1() {
		assertFalse("false", validator.isValid((double)0, null));
	}
	@Test
	public void testLongitudeValidatorDoubleFalse2() {
		assertFalse("false", validator.isValid((double)181, null));
	}
	@Test
	public void testLongitudeValidatorDoubleFalse3() {
		assertFalse("false", validator.isValid((double)-181, null));
	}

	// NULL
	public void testLongitudeValidatorNull() {
		assertFalse("false", validator.isValid(null, null));
	}
}
