package se.plushogskolan.jee.utils.validation;

import java.util.logging.Logger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class LongitudeValidator implements
ConstraintValidator<Longitude, Object> {

	private static Logger log = Logger.getLogger(LongitudeValidator.class.getName());

	@Override
	public void initialize(Longitude longitude) {
	}

	@Override
	public boolean isValid(Object longitude, ConstraintValidatorContext arg1) {
		log.fine("isValid called, Longitude value = " + longitude);

		double longitudeTyped = 0;

		if(longitude instanceof String) {
			try {
				longitudeTyped = Double.parseDouble((String) longitude);
			} catch (NumberFormatException nfe) {
				return false;
			}
		}
		if(longitude instanceof Double) {
			longitudeTyped = (Double) longitude;
		}

		if(longitudeTyped == 0.0){
			return false;
		}

		if((longitudeTyped < -180) || (longitudeTyped > 180)) {
			return false;
		}
		return true;
	}
}
