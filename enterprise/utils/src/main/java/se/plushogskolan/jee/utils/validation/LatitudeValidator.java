package se.plushogskolan.jee.utils.validation;

import java.util.logging.Logger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LatitudeValidator implements
ConstraintValidator<Latitude, Object> {

	private static Logger log = Logger.getLogger(LongitudeValidator.class.getName());

	@Override
	public void initialize(Latitude latitude) {
	}

	@Override
	public boolean isValid(Object latitude, ConstraintValidatorContext arg1) {
		log.fine("isValid called, Latitude value = " + latitude);

		double latitudeTyped = 0;

		if(latitude instanceof String) {
			try {
				latitudeTyped = Double.parseDouble((String) latitude);
			} catch (NumberFormatException nfe) {
				return false;
			}
		}
		if(latitude instanceof Double) {
			latitudeTyped = (Double) latitude;
		}

		if(latitudeTyped == 0.0){
			return false;
		}

		if((latitudeTyped < -180) || (latitudeTyped > 180)) {
			return false;
		}
		return true;
	}
}