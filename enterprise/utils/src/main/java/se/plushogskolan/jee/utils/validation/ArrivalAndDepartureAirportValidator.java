package se.plushogskolan.jee.utils.validation;

import java.util.logging.Logger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ArrivalAndDepartureAirportValidator implements
ConstraintValidator<ArrivalAndDepartureAirport, ArrivalAndDepartureAirportHolder>{

	private static Logger log = Logger.getLogger(ArrivalAndDepartureAirportValidator.class.getName());
	
	@Override
	public void initialize(ArrivalAndDepartureAirport constraintAnnotation) {
	}

	@Override
	public boolean isValid(ArrivalAndDepartureAirportHolder value,
			ConstraintValidatorContext context) {
		
		log.fine("isValid is called. ArrivalAndDepartureAirportHolder value = "+ value);
		
		if((value.getDepartureAirportCode() == null) || (value.getArrivalAirportCode() == null)) {
			log.fine("airports null");
			return false;
		}

		if(value.getDepartureAirportCode().contentEquals(value.getArrivalAirportCode())) {
			log.fine("airports are same");			
			return false;
		}

		if((value.getDepartureAirportCode().isEmpty()) || (value.getArrivalAirportCode().isEmpty())) {
			log.fine("airports is empty");
			return false;
		}
		log.fine("airports are OK, returning true");
		return true;
	}

}
