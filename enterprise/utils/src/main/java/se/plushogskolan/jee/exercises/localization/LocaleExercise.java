package se.plushogskolan.jee.exercises.localization;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleExercise {

	private static String propertyFile = "localizationexercise";  
	
	// ok
	public String sayHello(Locale locale) {
		return getMessages(locale).getString("sayHello");
	}
	// ok
	public String sayName(String name, Locale locale) {
		return MessageFormat.format(getMessages(locale).getString("sayName"), name);
	}
	// ok
	public String saySomethingEnglish(Locale locale) {
		return getMessages(locale).getString("saySomethingEnglish");
	}

	// ok
	public String introduceYourself(int age, String city, String name, Locale locale) {
		return MessageFormat.format(getMessages(locale).getString("introduceYourself"), name, age, city);
		}
	// ok
	public Object sayApointmentDate(Date date, Locale locale) {
		MessageFormat format = new MessageFormat(getMessages(locale).getString("sayApointmentDate"), locale);
		Object[] args = {date};
		return format.format(args);
	}
	// ok
	public Object sayPrice(double price, Locale locale) {
		MessageFormat format = new MessageFormat(getMessages(locale).getString("sayPrice"), locale);
		Object[] args = {price};
		return format.format(args);
	}
	// ok
	public String sayFractionDigits(double no) {
		ResourceBundle bundle = ResourceBundle.getBundle(propertyFile);
		return MessageFormat.format(bundle.getString("printFractionDigits"), no);
	}
	
	public ResourceBundle getMessages(Locale locale) {
		ResourceBundle messages = ResourceBundle.getBundle(propertyFile, locale);
		return messages;
	}
}
