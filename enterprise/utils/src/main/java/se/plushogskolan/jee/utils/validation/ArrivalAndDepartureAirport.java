package se.plushogskolan.jee.utils.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;

@Target({ TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ArrivalAndDepartureAirportValidator.class)
@Documented
public @interface ArrivalAndDepartureAirport {

	String message() default "{validation.flightRequest.arrivalAndDeparture.invalid}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
