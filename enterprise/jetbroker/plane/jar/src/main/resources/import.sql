-- You can use this file to load seed data into the database using SQL statements
insert into airport (id, code, name, latitude, longitude) values (100, 'GBG', 'Gothenburg', 57.697, 11.98);
insert into airport (id, code, name, latitude, longitude) values (101, 'STM', 'Stockholm', 59.33, 18.06);
insert into planetype (id, code, name, maxNoOfPassengers, maxRangeKm, maxSpeedKmH, fuelConsumptionPerKm) values (1, 'B770', 'Boeing 770', 300, 9000, 890, 22);
insert into planetype (id, code, name, maxNoOfPassengers, maxRangeKm, maxSpeedKmH, fuelConsumptionPerKm) values (2, 'A450', 'Airbus 450', 250, 7400, 930, 19);