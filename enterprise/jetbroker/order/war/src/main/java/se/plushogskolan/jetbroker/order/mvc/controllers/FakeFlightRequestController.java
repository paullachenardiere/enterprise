package se.plushogskolan.jetbroker.order.mvc.controllers;

import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.integration.JmsSimulator;
import se.plushogskolan.jetbroker.order.service.AirportService;

/**
 * Fake controller creates a mocked flight request. Sending the flight request to "JmsSimulator".
 * @author PaulsMacbookPro
 *
 */
@Controller
public class FakeFlightRequestController {

	private static Logger log = Logger.getLogger(FakeFlightRequestController.class.getName());

	@Inject
	AirportService airportService;
	
	@Inject
	JmsSimulator jmsSimulator;

	@RequestMapping("/fake.html")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("fake");
		log.fine("FAKE INDEX CALLED");

		Random randomPassengers = new Random();
		int passangers = randomPassengers.nextInt(501) + 1;
		log.fine("randomPassengers "+ passangers);
		List<Airport> allAirports = airportService.getAllAirports();
		log.fine("Airport size"+ allAirports.size());
		Random randomAirport = new Random();
		int airport1 = randomAirport.nextInt(allAirports.size());
		int airport2 = randomAirport.nextInt(allAirports.size());
		if(airport1 == airport2) {
			while (airport1 == airport2){
			airport2 = randomAirport.nextInt(allAirports.size());
			}
		}
		log.fine("airport1 "+ airport1);
		log.fine("airport2 "+ airport2);
		
		FlightRequest flightRequest = new FlightRequest(passangers, allAirports.get(airport1).getCode(),
				allAirports.get(airport2).getCode(), DateTime.now());

		log.fine(flightRequest.toString());
		jmsSimulator.sendMessage(flightRequest);

		return modelAndView;
	}
}
