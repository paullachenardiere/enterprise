package se.plushogskolan.jetbroker.order.mvc.wrappers;

import java.text.DecimalFormat;

import se.plushogskolan.jetbroker.order.domain.Airplane;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

public class AirplaneWrapper {

	private Airplane airplane;
	private Airport departureAirport;
	private Airport arrivalAirport;
	private PlaneType planeType;
	private FlightRequest flightRequest;
	private double totalFuelPrice;
	private double travelTime;

	public AirplaneWrapper(Airplane airplane, Airport departureAirport, Airport arrivalAirport, 
			PlaneType planeType, FlightRequest flightRequest, double totalFuelPrice, double travelTime) {
		setAirplane(airplane);
		setDepartureAirport(departureAirport);
		setArrivalAirport(arrivalAirport);
		setPlaneType(planeType);
		setFlightRequest(flightRequest);
		setTotalFuelPrice(totalFuelPrice);
		setTravelTime(travelTime);
	}


	public Airplane getAirplane() {
		return airplane;
	}


	public void setAirplane(Airplane airplane) {
		this.airplane = airplane;
	}


	public PlaneType getPlaneType() {
		return planeType;
	}


	public void setPlaneType(PlaneType planeType) {
		this.planeType = planeType;
	}


	public FlightRequest getFlightRequest() {
		return flightRequest;
	}


	public void setFlightRequest(FlightRequest flightRequest) {
		this.flightRequest = flightRequest;
	}

	public double getTotalFuelPrice() {
		return totalFuelPrice;
	}

	public String getTotalFuelPriceNiceName() {
		DecimalFormat df = new DecimalFormat("#.0");
		return df.format(totalFuelPrice);
	}

	public void setTotalFuelPrice(double totalFuelPrice) {
		this.totalFuelPrice = totalFuelPrice;
	}

	public double getTravelTime() {
		return travelTime;
	}

	public String getTravelTimeNiceName() {
		DecimalFormat df = new DecimalFormat("#.0");
		return df.format(travelTime);
	}

	public void setTravelTime(double travelTime) {
		this.travelTime = travelTime;
	}

	public Airport getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(Airport departureAirport) {
		this.departureAirport = departureAirport;
	}

	public Airport getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(Airport arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	@Override
	public String toString() {
		return "AirplaneWrapper [airplane=" + airplane + ", departureAirport="
				+ departureAirport + ", arrivalAirport=" + arrivalAirport
				+ ", planeType=" + planeType + ", flightRequest="
				+ flightRequest + ", totalFuelPrice=" + totalFuelPrice
				+ ", travelTime=" + travelTime + "]";
	}
}
