package se.plushogskolan.jetbroker.order.mvc.beans;

import se.plushogskolan.jetbroker.order.domain.Airplane;

/**
 * Bean to create and edit Airplanes. 
 * @author PaulsMacbookPro
 *
 */
public class AirplaneBean {

	private long id;
	private String planeTypeCode;
	private String description;


	public void copyAirplaneValuesToBean(Airplane airplane) {
		setId(airplane.getId());
		setPlaneTypeCode(airplane.getPlaneTypeCode());
		setDescription(airplane.getDescription());
	}


	public void copyBeanToAirplaneValues(Airplane airplane) {
		airplane.setId(getId());
		airplane.setPlaneTypeCode(getPlaneTypeCode());
		airplane.setDescription(getDescription());
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode.trim();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description.trim();
	}

	@Override
	public String toString() {
		return "AirplaneBean [id=" + id + ", planeTypeCode=" + planeTypeCode
				+ ", description=" + description + "]";
	}

}
