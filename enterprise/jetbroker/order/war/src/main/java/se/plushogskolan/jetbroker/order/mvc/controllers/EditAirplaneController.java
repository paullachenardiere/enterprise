package se.plushogskolan.jetbroker.order.mvc.controllers;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airplane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.mvc.beans.AirplaneBean;
import se.plushogskolan.jetbroker.order.service.AirplaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

/**
 * Controller for creating and editing airplanes.
 * @author PaulsMacbookPro
 */
@Controller
@RequestMapping("/editAirplane")
public class EditAirplaneController {

	private static Logger log = Logger.getLogger(EditAirplaneController.class.getName());
	
	@Inject
	AirplaneService airplaneService;
	@Inject
	PlaneTypeService planeTypeService;
	
	
	//Get
		@RequestMapping( value = "/{id}", method = RequestMethod.GET)
		public ModelAndView index(@PathVariable long id) {
			ModelAndView modelAndView = new ModelAndView("editAirplane");
			AirplaneBean airplaneBean = new AirplaneBean();
			
			log.fine("AirplaneController - CALLED");
			
			List<PlaneType> allPlaneTypes = getPlaneTypeServiceService().getAllPlaneTypes();
			if(id > 0){
				Airplane airplane = getAirplaneService().findAirplaneById(id);
				airplaneBean.copyAirplaneValuesToBean(airplane);
			}			
			modelAndView.addObject("allPlaneTypes", allPlaneTypes);
			modelAndView.addObject("airplaneBean", airplaneBean);
			
			return modelAndView; 
		}
	
	
	
	
	
	//Post (Save or Update airplane) 
		@RequestMapping(value = "/{id}", method = RequestMethod.POST)
		public ModelAndView handleSubmit(@Valid AirplaneBean airplaneBean, BindingResult errors) {
			
			log.fine("AirplaneController - POST");
			log.fine(airplaneBean.toString());
			
			
			if(airplaneBean.getId() > 0) {
				Airplane airplane = getAirplaneService().findAirplaneById(airplaneBean.getId());
				airplaneBean.copyBeanToAirplaneValues(airplane);
				getAirplaneService().updateAirplane(airplane);
				log.fine("Updated airplane = "+ airplane.getDescription());
			} else {
				Airplane airplane = new Airplane(); 
				airplaneBean.copyBeanToAirplaneValues(airplane);
				getAirplaneService().createAirplane(airplane);
				log.fine("Created airplane = "+ airplane.getDescription());
			}
			
			return new ModelAndView("redirect:/index.html");
		}
	
	

	public AirplaneService getAirplaneService() {
		return airplaneService;
	}

	public PlaneTypeService getPlaneTypeServiceService() {
		return planeTypeService;
	}
	
}
