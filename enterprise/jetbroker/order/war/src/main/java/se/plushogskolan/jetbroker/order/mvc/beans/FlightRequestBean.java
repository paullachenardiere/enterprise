package se.plushogskolan.jetbroker.order.mvc.beans;

/**
 * Bean to handle and edit Flight request offers. 
 * @author PaulsMacbookPro
 *
 */
public class FlightRequestBean {

	private double offeredPrice;
	private long airplaneId;
	
	
	public double getOfferedPrice() {
		return offeredPrice;
	}
	public void setOfferedPrice(double offeredPrice) {
		this.offeredPrice = offeredPrice;
	}
	public long getAirplaneId() {
		return airplaneId;
	}
	public void setAirplaneId(long airplaneId) {
		this.airplaneId = airplaneId;
	}
	
	@Override
	public String toString() {
		return "FlightRequestBean [offeredPrice=" + offeredPrice
				+ ", airplaneId=" + airplaneId + "]";
	}
}
