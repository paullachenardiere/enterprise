package se.plushogskolan.jetbroker.order.mvc.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airplane;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.domain.RequestStatusEnum;
import se.plushogskolan.jetbroker.order.mvc.beans.FlightRequestBean;
import se.plushogskolan.jetbroker.order.mvc.wrappers.AirplaneWrapper;
import se.plushogskolan.jetbroker.order.mvc.wrappers.FlightRequestWrapper;
import se.plushogskolan.jetbroker.order.service.AirplaneService;
import se.plushogskolan.jetbroker.order.service.AirportService;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

/**
 * Controller for editing Flight requests.
 * @author PaulsMacbookPro
 */
@Controller
@RequestMapping("/editFlightRequest")
public class EditFlightRequestController {

	private static Logger log = Logger.getLogger(EditFlightRequestController.class.getName());

	@Inject
	AirplaneService airplaneService;
	@Inject
	PlaneTypeService planeTypeService;
	@Inject
	FlightRequestService flightRequestService;
	@Inject
	AirportService airportService;
	@Inject
	FuelPriceService fuelPriceService; 

	//Get
	@RequestMapping( value = "/{id}", method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {
		ModelAndView modelAndView = new ModelAndView("editFlightRequest");

		log.fine("EditFlightRequestController - GET CALLED");
		FlightRequest flightRequest = getFlightRequestService().getFlightRequest(id);
		FlightRequestBean flightRequestBean = new FlightRequestBean();

		if(flightRequest.getRequestStatus().equals(RequestStatusEnum.OFFER_SENT)) {
			flightRequestBean.setAirplaneId(flightRequest.getAirplane().getId());
			flightRequestBean.setOfferedPrice(flightRequest.getOfferedPrice());
		}
		
		HashMap<String, PlaneType> allPlaneTypes = new HashMap<String, PlaneType>();
		for(PlaneType pt : getPlaneTypeService().getAllPlaneTypes()) {
			allPlaneTypes.put(pt.getCode(), pt);
		}

		double currentFuelPrice = getFuelPriceService().getCurrentFuelPrice();
		List<Airplane> allAirplanes = getAirplaneService().getAllAirplanes();
		Airport departureAirport = getAirportService().getAirportWithCode(flightRequest.getDepartureAirportCode());
		Airport arrivalAirport = getAirportService().getAirportWithCode(flightRequest.getArrivalAirportCode());

		double flightDistance = getFlightRequestService().getFlightDistance(flightRequest);

		// Wrapper for flight request and additional data
		FlightRequestWrapper flightRequestWrapper
		= new FlightRequestWrapper(flightRequest, departureAirport, arrivalAirport, flightDistance);

		// Wrapper for plane and offer data
		List<AirplaneWrapper> airplaneWrappers = new ArrayList<AirplaneWrapper>();
		for(Airplane airplane : allAirplanes) {
			PlaneType planeType = allPlaneTypes.get(airplane.getPlaneTypeCode());
			double travelTime = planeType.getTravelTimeWithFlightDistance(flightDistance);
			double totalFuelPrice = getFuelPriceService().calculateFuelPrice(flightDistance, planeType);
			AirplaneWrapper airplaneWrapper
			= new AirplaneWrapper(airplane, departureAirport, arrivalAirport, planeType, flightRequest, totalFuelPrice, travelTime);
			airplaneWrappers.add(airplaneWrapper);
		}

		modelAndView.addObject("flightRequestBean", flightRequestBean);
		modelAndView.addObject("flightRequestWrapper", flightRequestWrapper);
		modelAndView.addObject("airplaneWrappers", airplaneWrappers);
		modelAndView.addObject("currentFuelPrice", currentFuelPrice);
		log.fine("airplaneWrappers " + airplaneWrappers.toString());
		log.fine("flightRequestWrapper " + flightRequestWrapper.toString());
		return modelAndView; 
	}


	//Post (edit flight request) 
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid FlightRequestBean bean, BindingResult errors, @PathVariable long id, @RequestParam String op) {
		log.fine("EditFlightRequestController - POST CALLED");

		FlightRequest flightRequest = getFlightRequestService().getFlightRequest(id);
		if(op.equals("Reject offer")) {
			flightRequest.setRequestStatus(RequestStatusEnum.REJECTED);
			flightRequest.setOfferedPrice(0);
			flightRequest.setAirplane(null);
			getFlightRequestService().updateFlightRequest(flightRequest);
			return new ModelAndView("redirect:/index.html");
		} else {
			Airplane airplane = getAirplaneService().findAirplaneById(bean.getAirplaneId());
			log.fine("BEAN = "+bean.toString());
			log.fine("flightRequest = "+flightRequest.toString());
			log.fine("airplaneID from bean = "+bean.getAirplaneId());
			log.fine("airplane = "+airplane.toString());
			flightRequest.setAirplane(airplane);
			flightRequest.setOfferedPrice(bean.getOfferedPrice());
			flightRequest.setRequestStatus(RequestStatusEnum.OFFER_SENT);
			getFlightRequestService().updateFlightRequest(flightRequest);

			return new ModelAndView("redirect:/index.html");
		}
	}

	public AirplaneService getAirplaneService() {
		return airplaneService;
	}
	public PlaneTypeService getPlaneTypeService() {
		return planeTypeService;
	}
	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}
	public AirportService getAirportService() {
		return airportService;
	}
	public FuelPriceService getFuelPriceService() {
		return fuelPriceService;
	}
}
