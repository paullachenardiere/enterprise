package se.plushogskolan.jetbroker.order.mvc.controllers;


import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airplane;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.AirplaneService;
import se.plushogskolan.jetbroker.order.service.AirportService;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;
/**
 * Index controller. Lists all incoming requests and all airplanes. 
 * @author PaulsMacbookPro
 */
@Controller
public class IndexController {

	private static Logger log = Logger.getLogger(IndexController.class.getName());
	
	@Inject
	AirplaneService airplaneService;
	@Inject
	FlightRequestService flightRequestService;
	@Inject
	PlaneTypeService planeTypeService;
	@Inject
	FuelPriceService fuelPriceService;
	@Inject 
	AirportService airportService;
	

	@RequestMapping("/index.html")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("index");
		log.fine("INDEX CALLED");
		
		List<Airplane> allAirplanes = getAirplaneService().getAllAirplanes();
		List<FlightRequest> allFlightRequests = getFlightRequestService().getAllFlightRequests();
		double currentFuelPrice = getFuelPriceService().getCurrentFuelPrice();
		
		HashMap<String, PlaneType> allPlaneTypes = new HashMap<String, PlaneType>();
		for(PlaneType pt : getPlaneTypeService().getAllPlaneTypes()) {
			allPlaneTypes.put(pt.getCode(), pt);
		}
		
		HashMap<String, Airport> allAirports = new HashMap<String, Airport>();
		for(Airport a : getAirportService().getAllAirports()) {
			allAirports.put(a.getCode(), a);
		}
		
		modelAndView.addObject("currentFuelPrice", currentFuelPrice);
		modelAndView.addObject("allAirplanes", allAirplanes);
		modelAndView.addObject("allFlightRequests", allFlightRequests);
		modelAndView.addObject("allPlaneTypes", allPlaneTypes);
		modelAndView.addObject("allAirports", allAirports);
		return modelAndView;
	}




	public AirportService getAirportService() {
		return airportService;
	}
	public AirplaneService getAirplaneService() {
		return airplaneService;
	}
	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}
	public PlaneTypeService getPlaneTypeService() {
		return planeTypeService;
	}
	public void setPlaneTypeService(PlaneTypeService planeTypeService) {
		this.planeTypeService = planeTypeService;
	}
	public FuelPriceService getFuelPriceService() {
		return fuelPriceService;
	}
}
