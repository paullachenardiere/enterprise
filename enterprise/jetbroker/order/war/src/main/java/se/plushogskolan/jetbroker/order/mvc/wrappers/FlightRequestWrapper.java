package se.plushogskolan.jetbroker.order.mvc.wrappers;

import java.text.DecimalFormat;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;

public class FlightRequestWrapper {

	private FlightRequest flightRequest;
	private Airport departureAirport;
	private Airport arrivalAirport;
	private double flightDistance;

	
	public FlightRequestWrapper (FlightRequest flightRequest, Airport departureAirport, Airport arrivalAirport, double flightDistance) {
		setFlightRequest(flightRequest);
		setDepartureAirport(departureAirport);
		setArrivalAirport(arrivalAirport);
		setFlightDistance(flightDistance);
	}
	
	public FlightRequest getFlightRequest() {
		return flightRequest;
	}
	
	public void setFlightRequest(FlightRequest flightRequest) {
		this.flightRequest = flightRequest;
	}
	
	public Airport getDepartureAirport() {
		return departureAirport;
	}
	
	public void setDepartureAirport(Airport departureAirport) {
		this.departureAirport = departureAirport;
	}
	
	public Airport getArrivalAirport() {
		return arrivalAirport;
	}
	
	public void setArrivalAirport(Airport arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
	
	public double getFlightDistance() {
		return flightDistance;
	}
	
	public String getFlightDistanceNiceName() {
		DecimalFormat df = new DecimalFormat("#.0");
		return df.format(flightDistance);
	}

	public void setFlightDistance(double flightDistance) {
		this.flightDistance = flightDistance;
	}
	
	@Override
	public String toString() {
		return "FlightRequestWrapper [flightRequest=" + flightRequest
				+ ", departureAirport=" + departureAirport
				+ ", arrivalAirport=" + arrivalAirport + ", flightDistance="
				+ flightDistance + "]";
	}
}
