<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

<style type="text/css">
</style>

</head>
<body>

	<jsp:include page="header.jsp" />
	<br>
	<img class="floatLeft"
		src="<%=request.getContextPath()%>/images/plane.png">
	<h2 class="underline">Create airplane</h2>


	<form:form commandName="airplaneBean">

		<table class="floatLeft" class="formTable">
			<tr>
				<th>ID</th>
				<td><form:hidden path="id" /> ${airplaneBean.id}</td>
			</tr>
			<tr>
				<th>Plane type</th>
				<td><form:select type="text" path="planeTypeCode">
						<form:option value=""></form:option>
						<c:forEach items="${allPlaneTypes}" var="planeType">
							<form:option value="${planeType.code}">
							${planeType.name}</form:option>
						</c:forEach>
					</form:select></td>
			</tr>
			<tr>
				<th>Description</th>
				<td><form:input type="text" path="description"
						value="${airplaneBean.description}" /></td>
			</tr>
			<tr>
				<td></td>

				<td><input type="submit" value="submit" /> <a
					href="<%=request.getContextPath()%>/index.html">Cancel</a></td>
			</tr>
		</table>

	</form:form>


</body>

</html>