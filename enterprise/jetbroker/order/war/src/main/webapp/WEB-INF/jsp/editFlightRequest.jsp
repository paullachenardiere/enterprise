<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

<style type="text/css">
</style>

</head>
<body>

	<jsp:include page="header.jsp" />
	<br>
	<img class="floatLeft"
		src="<%=request.getContextPath()%>/images/plane.png">
	<h2 class="underline">Edit flight request</h2>

	<table>
		<tr>
			<th>ID</th>
			<td>${flightRequestWrapper.flightRequest.id}</td>
		<tr>
			<th>Departure airport</th>
			<td>${flightRequestWrapper.departureAirport.niceName}</td>
		<tr>

			<th>Arrival airport</th>
			<td>${flightRequestWrapper.arrivalAirport.niceName}</td>
		<tr>
			<th>Date</th>
			<td>${flightRequestWrapper.flightRequest.dateTimeNiceName}</td>
		<tr>
			<th>Passengers</th>
			<td>${flightRequestWrapper.flightRequest.amountOfpassangers}</td>
		<tr>
			<th>Distance (Km)</th>
			<td>${flightRequestWrapper.flightDistanceNiceName}</td>
		<tr>
			<th></th>
		<tr>
			<th>Fuel cost (liter)</th>
			<td>${currentFuelPrice}</td>
	</table>

	<h2 class="underline">Our offer to customer</h2>

	<form:form commandName="flightRequestBean">

		<table class="dataTable">
			<tr>
				<th></th>
				<th>Plane</th>
				<th>No of passengers</th>
				<th>Max speed (km/h)</th>
				<th>Fuel consumption (l/km)</th>
				<th>Total fuel cost</th>
				<th>Travel time (min)</th>
			</tr>


			<c:forEach items="${airplaneWrappers}" var="airplaneWrapper">
				<tr>
					<td><form:radiobutton path="airplaneId" value="${airplaneWrapper.airplane.id}" /></td>
					<td>${airplaneWrapper.planeType.name}</td>
					<td>${airplaneWrapper.planeType.maxNoOfPassengers}</td>
					<td>${airplaneWrapper.planeType.maxSpeedKmH}</td>
					<td>${airplaneWrapper.planeType.fuelConsumptionPerKm}</td>
					<td>${airplaneWrapper.totalFuelPriceNiceName}</td>
					<td>${airplaneWrapper.travelTimeNiceName}</td>
				</tr>
			</c:forEach>
		</table>

		<h2>Offered price (SEK)</h2>
		<form:input type="text" path="offeredPrice" />

		<br>
		<input type="submit" name="op" value="submit" />
		<input type="submit" name="op" value="Reject offer" />
		<a href="<%=request.getContextPath()%>/index.html">Cancel</a>

	</form:form>
</body>

</html>