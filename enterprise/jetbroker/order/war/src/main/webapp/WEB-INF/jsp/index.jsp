<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

</head>
<body>
	<jsp:include page="header.jsp" />

	<br>
	<img class="floatLeft" src="images/flightrequest.png">
	<h2 class="underline">Flight requests</h2>
	<br>

	<table class="dataTable">
		<tr>
			<th>ID</th>
			<th>Status</th>
			<th>Departure airport</th>
			<th>Arrival airport</th>
			<th>Date</th>
			<th>No of passengers</th>
			<th>Offered price</th>
			<th>Offered plane</th>
			<th>Edit</th>
		</tr>

		<c:forEach items="${allFlightRequests}" var="flightRequest">
			<tr>
				<td>${flightRequest.id}</td>
				<td>${flightRequest.requestStatus.name}</td>
				<td>${allAirports[flightRequest.departureAirportCode].niceName}</td>
				<td>${allAirports[flightRequest.arrivalAirportCode].niceName}</td>
				<td>${flightRequest.dateTimeNiceName}</td>
				<td>${flightRequest.amountOfpassangers}</td>
				<td>${flightRequest.offeredPrice}</td>
				<td>${allPlaneTypes[flightRequest.airplane.planeTypeCode].name}</td>
				
				<td><a
					href="<%=request.getContextPath()%>/editFlightRequest/${flightRequest.id}.html">
						<img src="<%=request.getContextPath()%>/images/edit.png">
				</a></td>
			</tr>
		</c:forEach>
	</table>

	<br>
	<br>

	<img class="floatLeft" src="images/plane.png">
	<h2 class="underline">Our planes</h2>

	<a class="button" class="link" class="floatLeft"
		href="<%=request.getContextPath()%>/editAirplane/0.html">Create
		new airplane</a>

	<br>
	<br>

	<table class="dataTable">
		<tr>
			<th>ID</th>
			<th>Code</th>
			<th>Name</th>
			<th>Max passengers</th>
			<th>Range (km)</th>
			<th>Max speed (km/h)</th>
			<th>Description</th>
			<th>Edit</th>
		</tr>

		<c:forEach items="${allAirplanes}" var="airplane">
			<tr>
				<td>${airplane.id}</td>
				<td>${airplane.planeTypeCode}</td>
				<td>${allPlaneTypes[airplane.planeTypeCode].name}</td>
				<td>${allPlaneTypes[airplane.planeTypeCode].maxNoOfPassengers}</td>
				<td>${allPlaneTypes[airplane.planeTypeCode].maxRangeKm}</td>
				<td>${allPlaneTypes[airplane.planeTypeCode].maxSpeedKmH}</td>
				<td>${airplane.description}</td>
				<td><a
					href="<%=request.getContextPath()%>/editAirplane/${airplane.id}.html">
						<img src="<%=request.getContextPath()%>/images/edit.png">
				</a></td>
			</tr>
		</c:forEach>
	</table>

	<br>
	<br>

	<img class="floatLeft" src="images/gas.png">
	<h2 class="underline">Fuel price</h2>
	<p>Current fuel cost is ${currentFuelPrice} SEK.</p>
</body>

</html>