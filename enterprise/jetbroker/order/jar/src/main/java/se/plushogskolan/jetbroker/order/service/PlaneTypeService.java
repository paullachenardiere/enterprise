package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.PlaneType;

@Local
public interface PlaneTypeService {
	
	PlaneType getPlaneTypeWithCode(String planeTypeCode);
	
	List<PlaneType> getPlaneTypeWithName(String name);
	
	List <PlaneType> getAllPlaneTypes();
	

}
