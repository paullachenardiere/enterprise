package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.Airport;

public interface AirportRepo {

	Airport getAirportWithCode(String airportCode);
	
	Airport getAirportWithName(String name);
	
	List <Airport> getAllAirports();
	
	
}
