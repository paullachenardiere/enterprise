package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Airplane.class are an @Entity in the Order artifact.
 * Airplane holds a reference to PlaneType.class (@Entity in the "Plane" artifact) with ${planeTypeCode}. 
 * @author PaulsMacbookPro
 */
@Entity
public class Airplane implements IdHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String planeTypeCode;
	private String description;

	public Airplane () {}

	public Airplane (String planeTypeCode) {
		setPlaneTypeCode(planeTypeCode);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode.trim();
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description.trim();
	}

	@Override
	public String toString() {
		return "Airplane [id=" + id + ", planeTypeCode=" + planeTypeCode
				+ ", description=" + description + "]";
	}
	
}
