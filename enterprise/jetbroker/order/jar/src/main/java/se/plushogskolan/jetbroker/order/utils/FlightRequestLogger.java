package se.plushogskolan.jetbroker.order.utils;

import java.util.logging.Logger;

import javax.enterprise.event.Observes;

import se.plushogskolan.jetbroker.order.annotations.Prod;
import se.plushogskolan.jetbroker.order.integration.events.NewFlightRequestEvent;

/**
 * Logger class or flight request. Currently only counts valid incoming requests.  
 * @author PaulsMacbookPro
 */
public class FlightRequestLogger {
	
	private static final Logger log = Logger.getLogger(FlightRequestLogger.class.getName());

	/**
	 * Counts incoming flight requests.
	 */
	private static int count;
	
	public void log(@Observes @Prod NewFlightRequestEvent event) {
		
		count++;
		
		log.fine("*** FlightRequestLogger ***");
		log.fine("No of incoming flight requests: " + count);
		log.fine("***************************");
	}
	
}
