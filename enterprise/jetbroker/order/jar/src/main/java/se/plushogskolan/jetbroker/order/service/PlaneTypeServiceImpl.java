package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepo;
/**
 * Service for planeType mocked data. 
 * @author PaulsMacbookPro
 */
@Stateless
public class PlaneTypeServiceImpl implements PlaneTypeService {

	@Inject
	private PlaneTypeRepo repo;

	@Override
	public PlaneType getPlaneTypeWithCode(String planeTypeCode) {
		return getRepo().getPlaneTypeWithCode(planeTypeCode);
	}

	@Override
	public List<PlaneType> getPlaneTypeWithName(String name) {
		return getRepo().getPlaneTypeWithName(name);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return getRepo().getAllPlaneTypes();
	}

	private PlaneTypeRepo getRepo() {
		return repo;
	}

	// EasyMock needs this. 
	public void setRepo(PlaneTypeRepo repo) {
		this.repo = repo;
	}
}