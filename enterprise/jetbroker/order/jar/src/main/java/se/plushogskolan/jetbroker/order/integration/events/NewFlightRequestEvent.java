package se.plushogskolan.jetbroker.order.integration.events;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;

/**
 * Event object. Holds a Flight request. 
 * @author PaulsMacbookPro
 *
 */
public class NewFlightRequestEvent {

	private FlightRequest flightRequest;

	public NewFlightRequestEvent(FlightRequest flightRequest) {
		setFlightRequest(flightRequest);
	}

	public FlightRequest getFlightRequest() {
		return flightRequest;
	}

	public void setFlightRequest(FlightRequest flightRequest) {
		this.flightRequest = flightRequest;
	} 

}
