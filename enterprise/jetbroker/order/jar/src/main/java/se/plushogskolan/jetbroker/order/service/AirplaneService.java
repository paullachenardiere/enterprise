package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Airplane;

@Local
public interface AirplaneService {

	long createAirplane(Airplane airplane);

	Airplane findAirplaneById(long id);

	void updateAirplane(Airplane airplane);

	void removeAirplane(Airplane airplane);

	List<Airplane> getAllAirplanes();



}
