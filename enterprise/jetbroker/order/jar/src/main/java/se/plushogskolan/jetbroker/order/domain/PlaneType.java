package se.plushogskolan.jetbroker.order.domain;

/**
 * PlaneType.class are not an @Entity in the Order artifact.
 * Airplane.class holds the object with an @Transient annotation. No @Persistence. 
 * The String variable "code" are instead used to pair with the object Airplane.class. 
 *  
 * @author PaulsMacbookPro
 */
public class PlaneType {

	private String code;
	private String name;
	private int maxNoOfPassengers;
	private int maxRangeKm;
	private int maxSpeedKmH;
	private double fuelConsumptionPerKm;
	
	public PlaneType(String code, String name,
			int maxNoOfPassengers, int maxRangeKm, int maxSpeedKmH, double fuelConsumptionPerKm) {

		setCode(code);
		setName(name);
		setMaxNoOfPassengers(maxNoOfPassengers);
		setMaxRangeKm(maxRangeKm);
		setMaxSpeedKmH(maxSpeedKmH);
		setFuelConsumptionPerKm(fuelConsumptionPerKm);
	}

	public PlaneType(String code, String name) {
		setCode(code);
		setName(name);
	}

	public double getTravelTimeWithFlightDistance(double flightDistance) {
		return flightDistance / getMaxSpeedKmH() * 60;
	}
	
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public int getMaxNoOfPassengers() {
		return maxNoOfPassengers;
	}
	public int getMaxRangeKm() {
		return maxRangeKm;
	}
	public int getMaxSpeedKmH() {
		return maxSpeedKmH;
	}
	public double getFuelConsumptionPerKm() {
		return fuelConsumptionPerKm;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMaxNoOfPassengers(int maxNoOfPassengers) {
		this.maxNoOfPassengers = maxNoOfPassengers;
	}
	public void setMaxRangeKm(int maxRangeKm) {
		this.maxRangeKm = maxRangeKm;
	}
	public void setMaxSpeedKmH(int maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
	}
	public void setFuelConsumptionPerKm(double fuelConsumptionPerKm) {
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}
	
	@Override
	public String toString() {
		return "PlaneType [code=" + code + ", name=" + name
				+ ", maxNoOfPassengers=" + maxNoOfPassengers + ", maxRangeKm="
				+ maxRangeKm + ", maxSpeedKmH=" + maxSpeedKmH
				+ ", fuelConsumptionPerKm=" + fuelConsumptionPerKm + "]";
	}
}
