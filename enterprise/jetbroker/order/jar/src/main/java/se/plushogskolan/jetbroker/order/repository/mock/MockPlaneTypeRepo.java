package se.plushogskolan.jetbroker.order.repository.mock;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepo;

/**
 * A repository that holds mocked data for PlaneType. 
 * @author PaulsMacbookPro
 */
public class MockPlaneTypeRepo implements PlaneTypeRepo {

	private List<PlaneType> allPlaneTypes = new ArrayList<PlaneType>();

	public MockPlaneTypeRepo() {
		allPlaneTypes.add(new PlaneType("B737", "Boeing 737",230, 700, 600, 30));
		allPlaneTypes.add(new PlaneType("B730", "Boeing 730",60, 200, 900, 20));	
		allPlaneTypes.add(new PlaneType("A300", "Airbus A300",600, 222, 850, 50));
		allPlaneTypes.add(new PlaneType("B120", "Boeing 120",122, 100, 900, 24));
		allPlaneTypes.add(new PlaneType("S3000", "Sputnik 3000",250, 900, 1200, 33));
	}

	@Override
	public PlaneType getPlaneTypeWithCode(String planeTypeCode) {
		for (PlaneType pt : getAllPlaneTypes()) {
			if (pt.getCode().equalsIgnoreCase(planeTypeCode)) {
				return pt;
			}
		}
		return null;
	}

	@Override
	public List<PlaneType> getPlaneTypeWithName(String name) {
		List<PlaneType> planeMatches = new ArrayList<PlaneType>(); 
		for (PlaneType pt : getAllPlaneTypes()) {
			if (pt.getName().equalsIgnoreCase(name)) {
				planeMatches.add(pt);
			}
		}
		return planeMatches;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return allPlaneTypes;
	}
	
	@Override
	public String toString() {
		return "MockPlaneTypeRepo [allPlaneTypes=" + allPlaneTypes + "]";
	}

}
