package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Airplane;
import se.plushogskolan.jetbroker.order.repository.AirplaneRepo;

/**
 * Service for Airplane persistent data. 
 * Provides full CRUD.
 * @author PaulsMacbookPro
 */
@Stateless
public class AirplaneServiceImpl implements AirplaneService {

	@Inject
	private AirplaneRepo airplaneRepo;

	@Override
	public long createAirplane(Airplane airplane) {
		return getAirplaneRepo().persist(airplane);
	}

	@Override
	public Airplane findAirplaneById(long id) {
		return getAirplaneRepo().findById(id);
	}

	@Override
	public void updateAirplane(Airplane airplane) {
		getAirplaneRepo().update(airplane);
	}

	@Override
	public void removeAirplane(Airplane airplane) {
		getAirplaneRepo().remove(airplane);
	}

	@Override
	public List<Airplane> getAllAirplanes() {
		return getAirplaneRepo().getAllAirplanes();
	}

	private AirplaneRepo getAirplaneRepo() {
		return airplaneRepo;
	}
	// EasyMock needs this.
	public void setRepo(AirplaneRepo airplaneRepo) {
		this.airplaneRepo = airplaneRepo;
	}
}
