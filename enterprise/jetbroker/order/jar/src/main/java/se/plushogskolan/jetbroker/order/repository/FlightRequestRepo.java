package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;

public interface FlightRequestRepo extends BaseRepository<FlightRequest>{

	List<FlightRequest> getAllFlightRequests();
	

}
