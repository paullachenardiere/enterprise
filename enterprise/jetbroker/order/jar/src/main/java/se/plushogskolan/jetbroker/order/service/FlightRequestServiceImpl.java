package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.map.HaversineDistance;
import se.plushogskolan.jetbroker.order.annotations.Prod;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.RequestStatusEnum;
import se.plushogskolan.jetbroker.order.integration.events.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepo;

/**
 * Service for Flight requests persistent data. 
 * Contains business logic and full CRUD.
 * @author PaulsMacbookPro
 */
@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	@Inject
	private FlightRequestRepo repo;
	@Inject
	private AirportService airportService; 

	@Inject
	@Prod
	private Event<NewFlightRequestEvent> eventProd;

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return getRepo().getAllFlightRequests();
	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		return getRepo().findById(id);
	}

	@Override
	public long saveFlightRequest(FlightRequest flightRequest) {
		flightRequest.setRequestStatus(RequestStatusEnum.NEW);
		long id = repo.persist(flightRequest);
		NewFlightRequestEvent newFlightRequestEvent = new NewFlightRequestEvent(flightRequest);
		getEventProd().fire(newFlightRequestEvent);
		return id; 

	}

	@Override
	public void updateFlightRequest(FlightRequest flightRequest) {
		repo.update(flightRequest);
	}

	@Override
	public double getFlightDistance(FlightRequest flightRequest) {
		Airport departureAirport = getAirportService().getAirportWithCode(flightRequest.getDepartureAirportCode());
		Airport arrivalAirport = getAirportService().getAirportWithCode(flightRequest.getArrivalAirportCode());
		return HaversineDistance.getDistance(departureAirport.getLatitude(), departureAirport.getLongitude(), 
				arrivalAirport.getLatitude(), arrivalAirport.getLongitude());
	}

	private FlightRequestRepo getRepo() {
		return repo;
	}
	private AirportService getAirportService() {
		return airportService;
	}
	public void setAirportService(AirportService airportService) {
		this.airportService = airportService;
	}
	// EasyMock needs this.
	public void setRepo(FlightRequestRepo repo) {
		this.repo = repo;
	}

	public Event<NewFlightRequestEvent> getEventProd() {
		return eventProd;
	}

	public void setEventProd(Event<NewFlightRequestEvent> eventProd) {
		this.eventProd = eventProd;
	}
}
