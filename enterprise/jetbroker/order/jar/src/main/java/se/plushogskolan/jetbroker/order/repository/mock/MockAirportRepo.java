package se.plushogskolan.jetbroker.order.repository.mock;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepo;

/**
 * A repository that holds mocked data for Airport. 
 * @author PaulsMacbookPro
 */
public class MockAirportRepo implements AirportRepo {

	private List<Airport> allAirports = new ArrayList<Airport>();

	public MockAirportRepo() {
		allAirports.add(new Airport("LVT", "Gothenburg", 57.66880, 12.29231 ));
		allAirports.add(new Airport("GCP", "Gothenburg", 57.77795, 11.86474));
		allAirports.add(new Airport("JFK", "New York", 40.64131, -73.77814));
		allAirports.add(new Airport("NYK", "Nykoping", 58.71154 , 16.93854));
		allAirports.add(new Airport("ARL", "Stockholm", 59.64976, 17.92378));
		allAirports.add(new Airport("CDG", "Paris", 48.84817, 2.36586 ));
		
		
	}

	@Override
	public Airport getAirportWithCode(String airportCode) {
		for (Airport a : getAllAirports()) {
			if (a.getCode().equalsIgnoreCase(airportCode)) {
				return a;
			}
		}
		return null;
	}

	@Override
	public Airport getAirportWithName(String name) {
		for (Airport a : getAllAirports()) {
			if (a.getName().equalsIgnoreCase(name)) {
				return a;
			}
		}
		return null;
	}

	@Override
	public List<Airport> getAllAirports() {
		return allAirports;
	}

	@Override
	public String toString() {
		return "MockAirportRepo [allAirports=" + allAirports + "]";
	}


}
