package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * This flight request class are used for incoming requests from the Agent artifact.
 * The enum "RequestStatus" confirms the currrent state of a flight request.
 * Possible values are:
 *	NEW
 *	REJECTED
 *	OFFER_SENT    
 * @author PaulsMacbookPro
 */
@Entity
public class FlightRequest implements IdHolder{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long agentId;
	private int amountOfpassangers; 
	private String departureAirportCode;
	private String arrivalAirportCode;
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime dateTime;
	@Enumerated(EnumType.ORDINAL)
	private RequestStatusEnum requestStatus;
	@ManyToOne
	@JoinColumn(name = "airplaneId")
	private Airplane airplane;
	private double offeredPrice;

	/**
	 * empty constructor for JPA. 
	 */
	public FlightRequest () {
	}

	/**
	 * Constructor for flight request.  
	 */
	public FlightRequest (int amountOfpassangers, String departureAirportCode, String arrivalAirportCode, DateTime dateTime)  {
		setAmountOfpassangers(amountOfpassangers);
		setDepartureAirportCode(departureAirportCode);
		setArrivalAirportCode(arrivalAirportCode);
		setDateTime(dateTime);
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getAgentId() {
		return agentId;
	}
	
	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	public int getAmountOfpassangers() {
		return amountOfpassangers;
	}

	public void setAmountOfpassangers(int amountOfpassangers) {
		this.amountOfpassangers = amountOfpassangers;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode.trim();
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode.trim();
	}

	public DateTime getDateTime() {
		return dateTime;
	}

	public String getDateTimeNiceName() {
		DateTimeFormatter dtf = DateTimeFormat.forPattern("Y-MM-dd HH:mm");
		return dtf.print(getDateTime());
	}

	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}

	public RequestStatusEnum getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatusEnum requestStatus) {
		this.requestStatus = requestStatus;
	}

	public double getOfferedPrice() {
		return offeredPrice;
	}
	
	public Airplane getAirplane() {
		return airplane;
	}

	public void setAirplane(Airplane airplane) {
		this.airplane = airplane;
	}

	public void setOfferedPrice(double offeredPrice) {
		this.offeredPrice = offeredPrice;
	}
	@Override
	public String toString() {
		return "FlightRequest [id=" + id + ", agentId=" + agentId
				+ ", amountOfpassangers=" + amountOfpassangers
				+ ", departureAirportCode=" + departureAirportCode
				+ ", arrivalAirportCode=" + arrivalAirportCode + ", dateTime="
				+ dateTime + ", requestStatus=" + requestStatus + ", airplane="
				+ airplane + ", offeredPrice=" + offeredPrice + "]";
	}

}
