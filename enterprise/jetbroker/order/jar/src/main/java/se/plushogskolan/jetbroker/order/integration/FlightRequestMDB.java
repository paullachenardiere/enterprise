package se.plushogskolan.jetbroker.order.integration;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.annotations.Mock;
import se.plushogskolan.jetbroker.order.integration.events.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.integration.facade.Confirmation;
import se.plushogskolan.jetbroker.order.integration.facade.OrderFacade;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;

/**
 * Flight request MDB. Currently listens to an event that contains a new (@Mock) incoming flight request.
 * Sends a confirmation to the order facade with object "Confirmation".
 * @author PaulsMacbookPro
 */
public class FlightRequestMDB {

	@Inject
	FlightRequestService flightRequestService;
	@Inject
	OrderFacade orderComfirmation;
	
	public void onMessage(@Observes @Mock NewFlightRequestEvent event) {
		long savedFlightRequest = getFlightRequestService().saveFlightRequest(event.getFlightRequest());
		orderComfirmation.sendConfirmation(new Confirmation(savedFlightRequest, event.getFlightRequest().getAgentId()));
	}


	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}
	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}
}
