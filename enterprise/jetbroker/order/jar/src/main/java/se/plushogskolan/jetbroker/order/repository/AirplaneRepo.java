package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.order.domain.Airplane;


public interface AirplaneRepo extends BaseRepository<Airplane>{

	List<Airplane> getAllAirplanes();
}
