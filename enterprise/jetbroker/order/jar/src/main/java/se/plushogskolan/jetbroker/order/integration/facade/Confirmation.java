package se.plushogskolan.jetbroker.order.integration.facade;

/**
 * Confirmation object for the integration facade (OrderFaceade). 
 * @author PaulsMacbookPro
 *
 */
public class Confirmation {

	long confirmationId;
	long agentId;
	
	
	public Confirmation (long confirmationId, long agentId) {
		setAgentId(agentId);
		setConfirmationId(confirmationId);
	}


	public long getConfirmationId() {
		return confirmationId;
	}


	public void setConfirmationId(long id) {
		this.confirmationId = id;
	}


	public long getAgentId() {
		return agentId;
	}


	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}


	@Override
	public String toString() {
		return "Confirmation [confirmationId=" + confirmationId + ", agentId="
				+ agentId + "]";
	}



}	
