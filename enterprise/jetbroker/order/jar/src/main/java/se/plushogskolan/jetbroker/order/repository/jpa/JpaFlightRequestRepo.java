package se.plushogskolan.jetbroker.order.repository.jpa;

import java.util.List;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepo;

/**
 * A persistent JPA repository for Flight requests CRUD functionality.  
 * @author PaulsMacbookPro
 */
public class JpaFlightRequestRepo extends JpaRepository<FlightRequest> implements FlightRequestRepo{

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getAllFlightRequests() {

		return em.createQuery("select fr from FlightRequest fr").getResultList();
	}


}
