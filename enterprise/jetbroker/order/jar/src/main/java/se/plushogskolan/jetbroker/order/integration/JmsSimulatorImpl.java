package se.plushogskolan.jetbroker.order.integration;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.annotations.Mock;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.integration.events.NewFlightRequestEvent;

/**
 * A simulator service. Called by a fake controller.  
 * @author PaulsMacbookPro
 *
 */
@Stateless
public class JmsSimulatorImpl implements JmsSimulator{

	@Inject
	FlightRequestMDB flightRequestMDB;
	
	@Inject
	@Mock
	Event<NewFlightRequestEvent> event;
	
	@Override
	public void sendMessage(FlightRequest flightRequest) {
		NewFlightRequestEvent newFlightRequestEvent = new NewFlightRequestEvent(flightRequest);
		event.fire(newFlightRequestEvent);
	}
}
