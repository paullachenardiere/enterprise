package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepo;

/**
 * Service for Airports mocked data. 
 * @author PaulsMacbookPro
 */
@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	private AirportRepo repo;

	@Override
	public Airport getAirportWithCode(String airportCode) {
		return getRepo().getAirportWithCode(airportCode);
	}

	@Override
	public Airport getAirportWithName(String name) {
		return getRepo().getAirportWithName(name);
	}

	@Override
	public List<Airport> getAllAirports() {
		return getRepo().getAllAirports();
	}

	private AirportRepo getRepo() {
		return repo;
	}

	// EasyMock needs this.
	public void setRepo(AirportRepo repo) {
		this.repo = repo;
	}

}
