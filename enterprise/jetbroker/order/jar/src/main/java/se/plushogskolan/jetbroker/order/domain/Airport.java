package se.plushogskolan.jetbroker.order.domain;

/**
 * Airport.class are not an @Entity in the Order artifact.
 * @author PaulsMacbookPro
 */
public class Airport implements Comparable<Airport> {

	private String code;
	private String name;
	private double latitude;
	private double longitude;

	public Airport(String code, String name, double latitude, double longitude) {
		setCode(code);
		setName(name);
		setLatitude(latitude);
		setLongitude(longitude);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Offers a easy way of printing out the full name of an airport.
	 */
	public String getNiceName() {
		return String.format("%s (%s)", getName(), getCode());
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Airport [code=" + code + ", name=" + name + ", latitude="
				+ latitude + ", longitude=" + longitude + "]";
	}

	@Override
	public int compareTo(Airport o) {
		return getName().compareToIgnoreCase(o.getName());
	}

}
