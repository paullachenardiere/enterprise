package se.plushogskolan.jetbroker.order.domain;
/**
 * RequestStatus confirms the currrent state of a flight request in the order artifact.
 *	REJECTED
	NEW
	OFFER_SENT
 * @author PaulsMacbookPro
 */
public enum RequestStatusEnum {

	REJECTED("Rejected"),
	NEW("New"),
	OFFER_SENT("Offer sent");

	private final String name;

	private RequestStatusEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
