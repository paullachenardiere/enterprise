package se.plushogskolan.jetbroker.order.integration;


import se.plushogskolan.jetbroker.order.domain.FlightRequest;

public interface JmsSimulator {

	void sendMessage (FlightRequest flightRequest);
	
}
