package se.plushogskolan.jetbroker.order.service;

import javax.ejb.Lock;
import javax.ejb.LockType;

import se.plushogskolan.jetbroker.order.domain.PlaneType;

public interface FuelPriceService {

	@Lock(LockType.READ)
	double getCurrentFuelPrice();
	
	@Lock(LockType.WRITE)
	void setFuelPrice(double fuelPrice);

	double calculateFuelPrice(double flightDistance, PlaneType planeType);
}
