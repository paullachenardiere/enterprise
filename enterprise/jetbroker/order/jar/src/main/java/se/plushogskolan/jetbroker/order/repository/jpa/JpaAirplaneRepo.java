package se.plushogskolan.jetbroker.order.repository.jpa;

import java.util.List;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.order.domain.Airplane;
import se.plushogskolan.jetbroker.order.repository.AirplaneRepo;

/**
 * A persistent JPA repository for Airplane CRUD functionality.  
 * @author PaulsMacbookPro
 */
public class JpaAirplaneRepo extends JpaRepository<Airplane> implements AirplaneRepo {

	@SuppressWarnings("unchecked")
	@Override
	public List<Airplane> getAllAirplanes() {

		return em.createQuery("select p from Airplane p").getResultList();
	}
	

}
