package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.PlaneType;

public interface PlaneTypeRepo {

	PlaneType getPlaneTypeWithCode(String planeTypeCode);

	List<PlaneType> getPlaneTypeWithName(String name);

	List <PlaneType> getAllPlaneTypes();


}
