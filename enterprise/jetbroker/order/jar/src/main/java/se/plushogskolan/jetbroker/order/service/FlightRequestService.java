package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;

public interface FlightRequestService {

	List<FlightRequest> getAllFlightRequests();

	FlightRequest getFlightRequest(long id);

	long saveFlightRequest(FlightRequest flightRequest);
	
	void updateFlightRequest(FlightRequest flightRequest);

	double getFlightDistance(FlightRequest flightRequest);
	
}
