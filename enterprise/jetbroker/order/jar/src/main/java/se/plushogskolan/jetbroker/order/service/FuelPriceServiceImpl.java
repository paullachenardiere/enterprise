package se.plushogskolan.jetbroker.order.service;

import javax.ejb.Singleton;
import javax.ejb.Startup;

import se.plushogskolan.jetbroker.order.domain.PlaneType;

/**
 * Service for fuel price mocked data. 
 * @author PaulsMacbookPro
 */

@Startup
@Singleton
public class FuelPriceServiceImpl implements FuelPriceService {

	private double fuelPrice = 7;
	
	@Override
	public double getCurrentFuelPrice() {
		return fuelPrice;
	}
	
	@Override
	public void setFuelPrice(double fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	@Override
	public double calculateFuelPrice(double flightDistance, PlaneType planeType) {
		return flightDistance * planeType.getFuelConsumptionPerKm() * getCurrentFuelPrice();
	}


}