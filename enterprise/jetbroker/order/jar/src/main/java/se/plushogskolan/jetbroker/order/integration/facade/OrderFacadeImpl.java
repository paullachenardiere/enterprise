package se.plushogskolan.jetbroker.order.integration.facade;

import java.util.logging.Logger;

/**
 * Order facade for integration. 
 * @author PaulsMacbookPro
 *
 */
public class OrderFacadeImpl implements OrderFacade {

	private static Logger log = Logger.getLogger(OrderFacadeImpl.class.getName());
	
	
	@Override
	public void sendConfirmation(Confirmation confirmation) {
		
		log.fine("OrderFacade = " + confirmation.toString());

	}

}
