package se.plushogskolan.jetbroker.order.integration.facade;


public interface OrderFacade {

	void sendConfirmation(Confirmation confirmation); 

}
