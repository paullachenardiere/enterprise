package se.plushogskolan.jetbroker.order.domain;

import org.junit.Assert;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;

public class FlightRequestTest {

	@Test
	public void offeredPrice() {
		FlightRequest validFlightRequest = TestFixture.getValidFlightRequest();
		validFlightRequest.setOfferedPrice(10000);
		Assert.assertEquals(10000, validFlightRequest.getOfferedPrice(), 0);
	}
	
	@Test
	public void requestStatus() {
		FlightRequest validFlightRequest = TestFixture.getValidFlightRequest();
		validFlightRequest.setRequestStatus(RequestStatusEnum.NEW);
		Assert.assertNotSame(RequestStatusEnum.OFFER_SENT, validFlightRequest.getRequestStatus());
	}
	@Test
	public void agentId() {
		FlightRequest validFlightRequest = TestFixture.getValidFlightRequest();
		validFlightRequest.setAgentId(1);
		Assert.assertEquals(1, validFlightRequest.getAgentId());
	}
}
