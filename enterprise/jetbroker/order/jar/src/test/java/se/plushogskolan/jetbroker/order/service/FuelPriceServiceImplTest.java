package se.plushogskolan.jetbroker.order.service;


import junit.framework.Assert;

import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

public class FuelPriceServiceImplTest {

	@Test
	public void calculateFuelPriceTest() {

		FuelPriceServiceImpl service = new FuelPriceServiceImpl();

		PlaneType planType = TestFixture.getValidPlanType();
		double fuelConsumptionPerKm = 30;
		planType.setFuelConsumptionPerKm(fuelConsumptionPerKm);
		double flightDistance = 100;
		double currentFuelPrice = 9;
		service.setFuelPrice(currentFuelPrice);
		double totalFuelPriceIntern = service.calculateFuelPrice(flightDistance, planType);
		double totalFuelPriceExtern = flightDistance * fuelConsumptionPerKm * currentFuelPrice;

		Assert.assertEquals(totalFuelPriceExtern, totalFuelPriceIntern, 0);

	}

}
