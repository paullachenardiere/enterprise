package se.plushogskolan.jetbroker.order.service;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepo;

public class AirportServiceImplTest {

	@Test
	public void testAirportService() {
		
		AirportServiceImpl service = new AirportServiceImpl();
		AirportRepo repo = EasyMock.createMock(AirportRepo.class);
		
		Airport airport1 = TestFixture.getValidAirport();
		Airport airport2 = TestFixture.getValidAirport();
		airport1.setCode("111");
		airport1.setName("JFK");
		airport2.setCode("222");
		ArrayList<Airport> arrayList = new ArrayList<Airport>();
		arrayList.add(airport1);
		arrayList.add(airport2);
		
		EasyMock.expect(repo.getAirportWithCode("111")).andReturn(airport1);
		EasyMock.expect(repo.getAirportWithName("JFK")).andReturn(airport1);
		EasyMock.expect(repo.getAllAirports()).andReturn(arrayList);
		EasyMock.replay(repo);
		service.setRepo(repo);
		
		Airport airportWithCode = service.getAirportWithCode("111");
		Airport airportWithName = service.getAirportWithName("JFK");
		List<Airport> allAirports = service.getAllAirports();
		
		EasyMock.verify(repo);
		
		Assert.assertEquals( "111", airportWithCode.getCode());
		Assert.assertEquals( "JFK", airportWithName.getName());
		Assert.assertEquals( 2, allAirports.size());
	}
	
}
