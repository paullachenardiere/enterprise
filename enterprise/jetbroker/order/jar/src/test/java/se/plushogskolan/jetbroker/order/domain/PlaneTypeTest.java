package se.plushogskolan.jetbroker.order.domain;

import org.junit.Assert;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;

public class PlaneTypeTest {
	
	@Test
	public void TravelTimeWithFlightDistanceMATH(){
		PlaneType validPlanType = TestFixture.getValidPlanType();
		double flightDistance = 1234;
		double travelTimeExtern = flightDistance / validPlanType.getMaxSpeedKmH() * 60;
		double travelTimeIntern = validPlanType.getTravelTimeWithFlightDistance(flightDistance);
		Assert.assertEquals("TravelTimeWithFlightDistanceMATH", travelTimeExtern, travelTimeIntern, 0);
	}
	
	@Test
	public void TravelTimeWithFlightDistanceMATH2(){
		PlaneType validPlanType = TestFixture.getValidPlanType();
		double flightDistance = 1234;
		double travelTimeExtern = flightDistance / 500 * 60;
		validPlanType.setMaxSpeedKmH(500);
		double travelTimeIntern = validPlanType.getTravelTimeWithFlightDistance(flightDistance);
		Assert.assertEquals("TravelTimeWithFlightDistanceMATH2", travelTimeExtern, travelTimeIntern, 0);
	}
	
}
