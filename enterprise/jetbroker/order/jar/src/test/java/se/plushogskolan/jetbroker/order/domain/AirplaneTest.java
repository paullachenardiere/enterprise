package se.plushogskolan.jetbroker.order.domain;

import org.junit.Assert;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;

public class AirplaneTest {

	@Test
	public void testAirplane() {
		Airplane validAirplane = TestFixture.getValidAirplane();
		Assert.assertEquals(0, validAirplane.getId());
		validAirplane.setId(1);
		Assert.assertEquals(1, validAirplane.getId());
	}
}
