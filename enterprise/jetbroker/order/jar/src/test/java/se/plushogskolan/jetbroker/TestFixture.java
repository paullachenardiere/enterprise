package se.plushogskolan.jetbroker;

import java.util.logging.Logger;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.order.domain.Airplane;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

/**
 * Tempelate constructors for Airplane.class, PlaneType.class...
 * @author PaulsMacbookPro
 */
public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.getName());

	/**
	 * Create a new PlaneType for testing. No params in constructor.
	 * @return a ValidPlaneType. 
	 */
	@SuppressWarnings("unused")
	private static PlaneType getValidPlaneType() {
		PlaneType validPlaneType = TestFixture.getValidPlaneType();
		return validPlaneType;	
	}
	/**
	 * Create a new Airplane for testing. No params in constructor.
	 * @return a ValidAirplane. 
	 */
	public static Airplane getValidAirplane() {
		return new Airplane();
	}

	public static Airport getValidAirport() {
		return new Airport("ABC", "Valid airport name", 20, -20);
	}

	public static FlightRequest getValidFlightRequest() {
		return new FlightRequest(100, "AAA", "BBB", DateTime.now());
	}
	
	public static PlaneType getValidPlanType() {
		return new PlaneType("PLT", "Plane type name", 122, 600, 550, 20);
	}
	
	



	/**
	 * @return a virtual "war" file (Webarchive) for integration testing.
	 */
	public static Archive<?> createIntegrationTestArchive() {

		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "order_test.war").addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}
}
