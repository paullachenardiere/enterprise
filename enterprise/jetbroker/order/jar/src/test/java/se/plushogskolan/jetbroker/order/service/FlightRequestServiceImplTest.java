package se.plushogskolan.jetbroker.order.service;

import javax.enterprise.event.Event;

import org.easymock.EasyMock;

import junit.framework.Assert;

import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.RequestStatusEnum;
import se.plushogskolan.jetbroker.order.integration.events.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepo;

public class FlightRequestServiceImplTest {

	@Test
	public void saveFlightRequest() {

		@SuppressWarnings("unchecked")
		Event<NewFlightRequestEvent> event = EasyMock.createMock(Event.class); 
		FlightRequestServiceImpl service = new FlightRequestServiceImpl();
		service.setEventProd(event);
		FlightRequestRepo repo = EasyMock.createMock(FlightRequestRepo.class);
		FlightRequest flightRequest = TestFixture.getValidFlightRequest();
		flightRequest.setId(1);
		EasyMock.expect(repo.persist(flightRequest)).andReturn(1L);
		EasyMock.expect(repo.findById(1)).andReturn(flightRequest);
		event.fire(EasyMock.isA(NewFlightRequestEvent.class));
		EasyMock.replay(repo, event);
		service.setRepo(repo);
		long id = service.saveFlightRequest(flightRequest);
		FlightRequest savedFlightRequest = service.getFlightRequest(id);
		EasyMock.verify(repo, event);
		Assert.assertTrue(savedFlightRequest.getRequestStatus().equals(RequestStatusEnum.NEW));
		Assert.assertEquals(1, savedFlightRequest.getId());
	}

	@Test
	public void updateFlightRequest() {
		FlightRequestServiceImpl service = new FlightRequestServiceImpl();
		FlightRequestRepo repo = EasyMock.createMock(FlightRequestRepo.class);
		FlightRequest flightRequest = TestFixture.getValidFlightRequest();
		repo.update(flightRequest);
		EasyMock.replay(repo);
		service.setRepo(repo);
		service.updateFlightRequest(flightRequest);
		EasyMock.verify(repo);
	}

	@Test
	public void getFlightDistanceTest() {
		FlightRequestServiceImpl service = new FlightRequestServiceImpl();
		AirportService airportService = EasyMock.createMock(AirportService.class);
		FlightRequest flightRequest = TestFixture.getValidFlightRequest();
		Airport airport1 = new Airport("ABC", "airport1", 20, -50);
		Airport airport2 = new Airport("CDE", "airport2", 40, -60);
		flightRequest.setDepartureAirportCode("ABC");
		flightRequest.setArrivalAirportCode("CDE");
		EasyMock.expect(airportService.getAirportWithCode(airport1.getCode())).andReturn(airport1);
		EasyMock.expect(airportService.getAirportWithCode(airport2.getCode())).andReturn(airport2);
		EasyMock.replay(airportService);
		service.setAirportService(airportService);
		double flightDistance = service.getFlightDistance(flightRequest);
		EasyMock.verify(airportService);
		Assert.assertEquals("flightDistance ", flightDistance, 2419.40097935951, 0);
	}
}
