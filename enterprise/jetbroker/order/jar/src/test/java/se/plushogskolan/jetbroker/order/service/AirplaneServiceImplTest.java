package se.plushogskolan.jetbroker.order.service;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airplane;
import se.plushogskolan.jetbroker.order.repository.AirplaneRepo;

public class AirplaneServiceImplTest {

	@Test
	public void createAirplane() {
		AirplaneServiceImpl service = new AirplaneServiceImpl();	
		AirplaneRepo repo = EasyMock.createMock(AirplaneRepo.class);
		Airplane airplane = TestFixture.getValidAirplane();
		airplane.setId(1);
		EasyMock.expect(repo.persist(airplane)).andReturn(1L);
		EasyMock.replay(repo);
		service.setRepo(repo);
		long id = service.createAirplane(airplane);
		EasyMock.verify(repo);
		Assert.assertEquals(1, id);
	}

	@Test
	public void findAirplaneById() {
		AirplaneServiceImpl service = new AirplaneServiceImpl();	
		AirplaneRepo repo = EasyMock.createMock(AirplaneRepo.class);
		Airplane airplane = TestFixture.getValidAirplane();
		airplane.setId(1);
		EasyMock.expect(repo.findById(1)).andReturn(airplane);
		EasyMock.replay(repo);
		service.setRepo(repo);
		Airplane findAirplaneById = service.findAirplaneById(1);
		EasyMock.verify(repo);
		Assert.assertEquals(findAirplaneById, airplane);
	}
		@Test
		public void updateAirplane() {
			AirplaneServiceImpl service = new AirplaneServiceImpl();	
			AirplaneRepo repo = EasyMock.createMock(AirplaneRepo.class);
			Airplane airplane = TestFixture.getValidAirplane();
			repo.update(airplane);
			EasyMock.replay(repo);
			service.setRepo(repo);
			service.updateAirplane(airplane);
			EasyMock.verify(repo);
		}
		
		@Test
		public void getAllAirplanes() {
			AirplaneServiceImpl service = new AirplaneServiceImpl();	
			AirplaneRepo repo = EasyMock.createMock(AirplaneRepo.class);
			Airplane airplane1 = TestFixture.getValidAirplane();
			Airplane airplane2 = TestFixture.getValidAirplane();
			ArrayList<Airplane> allAirplanes = new ArrayList<Airplane>();
			allAirplanes.add(airplane1);
			allAirplanes.add(airplane2);
			EasyMock.expect(repo.getAllAirplanes()).andReturn(allAirplanes);
			EasyMock.replay(repo);
			service.setRepo(repo);
			List<Airplane> all = service.getAllAirplanes();
			EasyMock.verify(repo);
			Assert.assertEquals(2, all.size());
		}
}
