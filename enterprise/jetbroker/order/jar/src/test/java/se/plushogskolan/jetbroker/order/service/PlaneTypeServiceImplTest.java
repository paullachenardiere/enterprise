package se.plushogskolan.jetbroker.order.service;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepo;

public class PlaneTypeServiceImplTest {

	@Test
	public void testPlaneTypeService() {
		
		PlaneTypeServiceImpl service = new PlaneTypeServiceImpl();
		PlaneTypeRepo repo = EasyMock.createMock(PlaneTypeRepo.class);
		
		PlaneType planeType = new PlaneType("123", "Boeing 737");
		ArrayList<PlaneType> arrayList = new ArrayList<PlaneType>();
		arrayList.add(planeType);
		
		EasyMock.expect(repo.getPlaneTypeWithCode("123")).andReturn(planeType);
		EasyMock.expect(repo.getPlaneTypeWithName("Boeing 737")).andReturn(arrayList);
		EasyMock.replay(repo);
		service.setRepo(repo);
		
		PlaneType planeTypeWithCode = service.getPlaneTypeWithCode("123");
		List<PlaneType> planeTypeWithName = service.getPlaneTypeWithName("Boeing 737");
		
		EasyMock.verify(repo);
		
		Assert.assertEquals( "123", planeTypeWithCode.getCode());
		Assert.assertEquals( "Boeing 737", planeTypeWithName.get(0).getName());
	}
}
