package se.plushogskolan.jetbroker.order.repository.jpa;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airplane;

/**
 * Integration test for Airplane.class
 * @author PaulsMacbookPro
 */
@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaAirplaneRepoIntegrationTest  extends AbstractRepositoryTest<Airplane, JpaAirplaneRepo>{
	
	@Inject
	JpaAirplaneRepo repo;

	@Test
	public void testGetAllAirplanes() {
		
		long persist1 = repo.persist(TestFixture.getValidAirplane());
		long persist2 = repo.persist(TestFixture.getValidAirplane());
		Airplane savedAirplane1 = repo.findById(persist1);
		Airplane savedAirplane2 = repo.findById(persist2);
		Assert.assertEquals(persist1, savedAirplane1.getId());
		Assert.assertEquals(persist2, savedAirplane2.getId());
		Assert.assertEquals(2, repo.getAllAirplanes().size());
	}
	
	
	@Override
	protected Airplane getEntity1() {
		return TestFixture.getValidAirplane();
	}

	@Override
	protected Airplane getEntity2() {
		return TestFixture.getValidAirplane();
	}

	@Override
	protected JpaAirplaneRepo getRepository() {
		return repo;
	}
}
