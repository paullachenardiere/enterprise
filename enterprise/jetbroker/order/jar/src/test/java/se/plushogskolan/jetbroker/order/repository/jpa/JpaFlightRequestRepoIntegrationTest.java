package se.plushogskolan.jetbroker.order.repository.jpa;

import javax.inject.Inject;

import junit.framework.Assert;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaFlightRequestRepoIntegrationTest extends AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepo>{

	@Inject
	JpaFlightRequestRepo jpaFlightRequestRepo;

	@Test
	public void getAllFlightRequests() {

		long id1 = getJpaFlightRequestRepo().persist(TestFixture.getValidFlightRequest());
		long id2 = getJpaFlightRequestRepo().persist(TestFixture.getValidFlightRequest());
		FlightRequest flightRequest1 = getJpaFlightRequestRepo().findById(id1);
		FlightRequest flightRequest2 = getJpaFlightRequestRepo().findById(id2);
		Assert.assertEquals(id1, flightRequest1.getId());
		Assert.assertEquals(id2, flightRequest2.getId());
		Assert.assertEquals(2, getJpaFlightRequestRepo().getAllFlightRequests().size());
	}
	
	
	@Override
	protected JpaFlightRequestRepo getRepository() {
		return getJpaFlightRequestRepo();
	}

	@Override
	protected FlightRequest getEntity1() {
		return TestFixture.getValidFlightRequest();
	}

	@Override
	protected FlightRequest getEntity2() {
		return TestFixture.getValidFlightRequest();
	}

	public JpaFlightRequestRepo getJpaFlightRequestRepo() {
		return jpaFlightRequestRepo;
	}

	public void setJpaFlightRequestRepo(JpaFlightRequestRepo jpaFlightRequestRepo) {
		this.jpaFlightRequestRepo = jpaFlightRequestRepo;
	}

}
