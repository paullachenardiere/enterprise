package se.plushogskolan.jetbroker.agent.domain;

/**
 * Airport.class are not an @Entity in the Agent artifact.
 * Just for mocked data.
 * @author PaulsMacbookPro
 */
public class Airport implements Comparable<Airport> {

	private String code;
	private String name;

	public Airport(String code, String name) {
		setCode(code);
		setName(name);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Offers a easy way of printing out the full name of an airport.
	 */
	public String getNiceName() {
		return String.format("%s (%s)", getName(), getCode());
	}

	@Override
	public String toString() {
		return "Airport [code=" + code + ", name=" + name + "]";
	}

	@Override
	public int compareTo(Airport o) {
		return getName().compareToIgnoreCase(o.getName());
	}

}
