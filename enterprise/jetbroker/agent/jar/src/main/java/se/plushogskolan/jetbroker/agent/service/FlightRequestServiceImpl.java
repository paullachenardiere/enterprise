package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepo;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	@Inject
	FlightRequestRepo repo;

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return getRepo().getAllFlightRequests();
	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		return getRepo().findById(id);
	}

	@Override
	public FlightRequest createFlightRequest(FlightRequest flightRequest) {
		return getRepo().findById(getRepo().persist(flightRequest));
	}

	@Override
	public void updateFlightRequest(FlightRequest flightRequest) {
		getRepo().update(flightRequest);
	}

	@Override
	public void deleteFlightRequest(FlightRequest flightRequest) {
		getRepo().remove(flightRequest);
	}

	public FlightRequestRepo getRepo() {
		return repo;
	}

	public void setRepo(FlightRequestRepo repo) {
		this.repo = repo;
	}
}
