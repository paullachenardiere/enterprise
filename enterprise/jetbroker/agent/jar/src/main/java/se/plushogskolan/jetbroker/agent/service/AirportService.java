package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.Airport;

@Local
public interface AirportService {

	Airport getAirportWithCode(String airportCode);

	Airport getAirportWithName(String name);

	List <Airport> getAllAirports();

}
