package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.Customer;

@Local
public interface CustomerService {

	List<Customer> getAllCustomers();

	Customer getCustomer(long id);

	Customer createCustomer(Customer customer);

	void updateCustomer(Customer customer);

	void deleteCustomer(long id); 

}
