package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.Airport;

public interface AirportRepo {

	Airport getAirportWithCode(String airportCode);

	Airport getAirportWithName(String name);

	List <Airport> getAllAirports();


}
