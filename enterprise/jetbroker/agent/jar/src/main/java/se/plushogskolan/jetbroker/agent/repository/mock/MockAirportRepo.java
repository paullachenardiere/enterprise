package se.plushogskolan.jetbroker.agent.repository.mock;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.repository.AirportRepo;

/**
 * A repository that holds mocked data for Airport. 
 * @author PaulsMacbookPro
 */
public class MockAirportRepo implements AirportRepo {

	private List<Airport> allAirports = new ArrayList<Airport>();

	public void fillUpAirports() {
		allAirports.clear();
		allAirports.add(new Airport("LVT", "Gothenburg"));
		allAirports.add(new Airport("GCP", "Gothenburg"));
		allAirports.add(new Airport("JFK", "New York"));
		allAirports.add(new Airport("NYK", "Nykoping"));
		allAirports.add(new Airport("ARL", "Stockholm"));
		allAirports.add(new Airport("CDG", "Paris"));
	}

	@Override
	public Airport getAirportWithCode(String airportCode) {
		for (Airport a : getAllAirports()) {
			if (a.getCode().equalsIgnoreCase(airportCode)) {
				return a;
			}
		}
		return null;
	}

	@Override
	public Airport getAirportWithName(String name) {
		for (Airport a : getAllAirports()) {
			if (a.getName().equalsIgnoreCase(name)) {
				return a;
			}
		}
		return null;

	}

	@Override
	public List<Airport> getAllAirports() {
		fillUpAirports();
		return allAirports;
	}

	@Override
	public String toString() {
		return "MockAirportRepo [allAirports=" + allAirports + "]";
	}


}
