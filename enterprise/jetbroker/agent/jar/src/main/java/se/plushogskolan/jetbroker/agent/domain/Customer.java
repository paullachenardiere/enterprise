package se.plushogskolan.jetbroker.agent.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jee.utils.domain.IdHolder;
/**
 * Customer.class holds all customer information.
 * @author PaulsMacbookPro
 */
@Entity
public class Customer implements IdHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotBlank
	private String firstName = "";
	@NotBlank
	private String lastName = "";	
	@NotBlank
	@Email
	private String email = "";	
	private String company = "";

	/**
	 * empty constructor for JPA. 
	 */
	public Customer() {
	}

	/**
	 * Constructor for a simple customer. Only name required.  
	 */
	public Customer (String firstName, String lastname)  {
		setFirstName(firstName);
		setLastName(lastname);
	}

	/**
	 * Constructor for a complete customer. All param required.  
	 */
	public Customer (String firstName, String lastname, String email, String company)  {
		setFirstName(firstName);
		setLastName(lastname);
		setEmail(email);
		setCompany(company);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName.trim();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName.trim();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.trim();
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company.trim();
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public long getId() {
		return id;
	}
	/**
	 * Offers a easy way of printing out the full name of a customer.
	 */
	public String getNiceName() {
		return String.format("%s %s", getFirstName(), getLastName());
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", company="
				+ company + "]";
	}
}