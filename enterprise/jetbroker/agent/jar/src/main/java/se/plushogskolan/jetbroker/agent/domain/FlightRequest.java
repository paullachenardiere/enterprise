package se.plushogskolan.jetbroker.agent.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirport;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;

/**
 * A request class that contains all specific demands from the customer. 
 * {@link RequestStatusEnum} holds the current status of the request.    
 * @author PaulsMacbookPro
 */
@ArrivalAndDepartureAirport
@Entity
public class FlightRequest implements IdHolder, ArrivalAndDepartureAirportHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;
	
	@Range(min = 1, max = 500)
	private int amountOfpassangers; 
	
	@NotBlank
	private String departureAirportCode;
	
	@NotBlank
	private String arrivalAirportCode;
	
	@NotNull	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime dateTime;
	
	@Enumerated(EnumType.ORDINAL)
	@NotNull
	private RequestStatusEnum requestStatus;

	/**
	 * empty constructor for JPA. 
	 */
	public FlightRequest () {
		setRequestStatus(RequestStatusEnum.CREATED);
	}

	/**
	 * Constructor for flight request.  
	 */
	public FlightRequest (Customer customer, int amountOfpassangers, 
			String departureAirportCode, String arrivalAirportCode, DateTime dateTime)  {
		this();
		setCustomer(customer);
		setAmountOfpassangers(amountOfpassangers);
		setDepartureAirportCode(departureAirportCode);
		setArrivalAirportCode(arrivalAirportCode);
		setDateTime(dateTime);
	}



	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getAmountOfpassangers() {
		return amountOfpassangers;
	}

	public void setAmountOfpassangers(int amountOfpassangers) {
		this.amountOfpassangers = amountOfpassangers;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode.trim();
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode.trim();
	}

	public DateTime getDateTime() {
		return dateTime;
	}

	public String getDateTimeNiceName() {
		DateTimeFormatter dtf = DateTimeFormat.forPattern("Y-MM-dd HH:mm");
		return dtf.print(getDateTime());
	}

	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public long getId() {
		return id;
	}

	public RequestStatusEnum getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatusEnum requestStatus) {
		this.requestStatus = requestStatus;
	}

	@Override
	public String toString() {
		return "FlightRequest [id=" + id + ", customer=" + customer
				+ ", amountOfpassangers=" + amountOfpassangers
				+ ", departureAirportCode=" + departureAirportCode
				+ ", arrivalAirportCode=" + arrivalAirportCode + ", dateTime="
				+ dateTime + ", requestStatus=" + requestStatus + "]";
	}

}
