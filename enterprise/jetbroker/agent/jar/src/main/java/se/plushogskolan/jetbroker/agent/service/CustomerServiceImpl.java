package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepo;

@Stateless
public class CustomerServiceImpl implements CustomerService {

	@Inject
	private CustomerRepo customerRepo;

	@Override
	public List<Customer> getAllCustomers() {
		return getCustomerRepo().getAllCustomers();
	}

	@Override
	public Customer createCustomer(Customer customer) {
		long id = getCustomerRepo().persist(customer);
		return getCustomer(id);
	}

	@Override
	public Customer getCustomer(long id) {
		return getCustomerRepo().findById(id);
	}

	@Override
	public void updateCustomer(Customer customer) {
		getCustomerRepo().update(customer);
	}

	@Override
	public void deleteCustomer(long id) {
		getCustomerRepo().remove(getCustomer(id));
	}

	public CustomerRepo getCustomerRepo() {
		return customerRepo;
	}

}
