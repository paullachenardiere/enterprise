package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.repository.AirportRepo;

@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	AirportRepo repo;

	@Override
	public Airport getAirportWithCode(String airportCode) {
		return getRepo().getAirportWithCode(airportCode);
	}

	@Override
	public Airport getAirportWithName(String name) {
		return getRepo().getAirportWithName(name);
	}

	@Override
	public List<Airport> getAllAirports() {
		return getRepo().getAllAirports();
	}

	public AirportRepo getRepo() {
		return repo;
	}

	// EasyMock needs this.
	public void setRepo(AirportRepo repo) {
		this.repo = repo;
	}

}
