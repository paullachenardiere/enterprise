package se.plushogskolan.jetbroker.agent.domain;
/**
 * RequestStatus confirms the currrent state of a flight request.
 * 	CREATED
	REQUEST_CONFIRMED
	OFFER_RECEIVED
	REJECTED
 * @author PaulsMacbookPro
 */
public enum RequestStatusEnum {

	CREATED("Created"),
	REQUEST_CONFIRMED("Request confirmed"),
	OFFER_RECEIVED("Offer received"),
	REJECTED("Rejected");

	private final String name;

	private RequestStatusEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
