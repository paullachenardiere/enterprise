package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.Customer;

public interface CustomerRepo extends BaseRepository<Customer> {

	List<Customer> getAllCustomers();

}
