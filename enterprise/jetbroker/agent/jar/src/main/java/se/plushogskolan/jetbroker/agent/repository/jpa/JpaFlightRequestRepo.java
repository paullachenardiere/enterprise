package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.List;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepo;

public class JpaFlightRequestRepo extends JpaRepository<FlightRequest> implements FlightRequestRepo{

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getAllFlightRequests() {

		return em.createQuery("select fr from FlightRequest fr").getResultList();
	}


}
