package se.plushogskolan.jetbroker.agent.repository.jpa;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;

/**
 * Integration test for Customer.class
 * @author PaulsMacbookPro
 */
@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaCustomerRepoIntegrationTest extends AbstractRepositoryTest<Customer, JpaCustomerRepo>  {

	//Repository used for persistence (JPA) to Customer.class .
	@Inject
	JpaCustomerRepo repo;

	// Tests additional methods in repo.
	@Test
	public void testGetAllCustomers() {

		long persist1 = repo.persist(TestFixture.getValidCustomer());
		long persist2 = repo.persist(TestFixture.getValidCustomer());
		Customer validCustomer1Saved = repo.findById(persist1);
		Customer validCustomer2Saved = repo.findById(persist2);
		Assert.assertEquals(persist1, validCustomer1Saved.getId());
		Assert.assertEquals(persist2, validCustomer2Saved.getId());
		Assert.assertEquals(2, repo.getAllCustomers().size());
		Assert.assertEquals(persist1 < persist2, true);

		repo.remove(repo.findById(persist1));
		Assert.assertEquals(1, repo.getAllCustomers().size());
		repo.remove(repo.findById(persist2));
		Assert.assertEquals(0, repo.getAllCustomers().size());
	}
	
	@Test(expected = Exception.class)
	public void testCustomerJPAException() {
		// Customer is valid
		Customer customer = TestFixture.getValidCustomer();
		// Sets a forbidden value (empty name) ==> Customer is not longer valid.
		customer.setFirstName("");
		// Try to save. 
		repo.persist(customer);
	}

	@Override
	protected JpaCustomerRepo getRepository() {
		return repo;
	}

	// Creates valid customer 1. 
	@Override
	protected Customer getEntity1() {
		return TestFixture.getValidCustomer();
	}

	// Creates valid customer 2.
	@Override
	protected Customer getEntity2() {
		return TestFixture.getValidCustomer();
	}


}
