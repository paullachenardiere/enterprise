package se.plushogskolan.jetbroker.agent.domain;

import java.util.Set;
import javax.validation.ConstraintViolation;
import org.junit.Before;
import org.junit.Test;
import se.plushogskolan.jetbroker.PropertyValidator;
import se.plushogskolan.jetbroker.TestFixture;

/**
 * Integration test for Customer.class 
 * Tests basic functionality and validation.
 * @author PaulsMacbookPro
 */

public class CustomerValidationTest {
	
	PropertyValidator<Customer> validator;
	
	@Before
	public void init() {
		validator = new PropertyValidator<Customer>();
	}
	
	@Test
	public void blankFirstName() {
		Customer customer = TestFixture.getValidCustomer();
		customer.setFirstName("");
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(customer);
		validator.assertPropertyIsInvalid("firstName", violations);
	}
	@Test
	public void blankLastName() {
		Customer customer = TestFixture.getValidCustomer();
		customer.setLastName("");
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(customer);
		validator.assertPropertyIsInvalid("lastName", violations);
	}
	@Test
	public void blankEmail() {
		Customer customer = TestFixture.getValidCustomer();
		customer.setEmail("");
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(customer);
		validator.assertPropertyIsInvalid("email", violations);
	}
	@Test
	public void notValidEmail() {
		Customer customer = TestFixture.getValidCustomer();
		customer.setEmail("paul@");
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(customer);
		validator.assertPropertyIsInvalid("email", violations);
	}
	
}
