package se.plushogskolan.jetbroker.agent.domain;

import junit.framework.Assert;

import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;

/**
 * Unit test for Customer.class 
 * @author PaulsMacbookPro
 */
public class CustomerTest {

	@Test
	public void testCustomerId(){
		Customer customer = TestFixture.getValidCustomer(1);
		Assert.assertEquals(1, customer.getId());
	}
	@Test
	public void testCustomersGettersAnSetters(){
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setFirstName("firstname");
		customer.setLastName("lastname");
		customer.setEmail("email");
		customer.setCompany("company");
		Assert.assertEquals("firstname", customer.getFirstName());
		Assert.assertEquals("lastname", customer.getLastName());
		Assert.assertEquals("email", customer.getEmail());
		Assert.assertEquals("company", customer.getCompany());
	}
	
	@Test
	public void testEmptyCustomer(){
		Customer customer = new Customer();
		Assert.assertEquals(0, customer.getId());
		Assert.assertEquals("firstname","", customer.getFirstName());
		Assert.assertEquals("lastname","", customer.getLastName());
		Assert.assertEquals("email","", customer.getEmail());
		Assert.assertEquals("company","", customer.getCompany());
	}
	@Test
	public void testCustomerFullConstructor(){
		Customer customer = new Customer("first","last","email","company");
		Assert.assertEquals(0, customer.getId());
		Assert.assertEquals("firstname","first", customer.getFirstName());
		Assert.assertEquals("lastname","last", customer.getLastName());
		Assert.assertEquals("email","email", customer.getEmail());
		Assert.assertEquals("company","company", customer.getCompany());
	}
	@Test
	public void testCustomerSmallConstructorFirstNameAndLastName(){
		Customer customer = new Customer("first","last");
		Assert.assertEquals("firstname","first", customer.getFirstName());
		Assert.assertEquals("lastname","last", customer.getLastName());
		Assert.assertEquals("email","", customer.getEmail());
		Assert.assertEquals("company","", customer.getCompany());
	}
}
