package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.logging.Logger;

import javax.inject.Inject;

import junit.framework.Assert;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.RequestStatusEnum;

/**
 * Integration test for FlightRequest.class
 * @author PaulsMacbookPro
 */
@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaFlightRequestRepoIntegrationTest extends AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepo>{

	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(JpaFlightRequestRepoIntegrationTest.class.getName());

	@Inject
	JpaFlightRequestRepo flightRequestRepo;
	@Inject
	JpaCustomerRepo customerRepo;
	
	// Creates and saves a customer for testing purposes.
	public Customer createAndSaveCustomer() {
		return customerRepo.findById(customerRepo.persist(new Customer("FirstName","LastName","poplabbet@gmail.com","Company")));
	}
	
	@Test
	public void testFlightRequest() {
		Customer customer = createAndSaveCustomer();
		FlightRequest validFlightRequest = TestFixture.getValidFlightRequest(customer);
		long flightRequestid = flightRequestRepo.persist(validFlightRequest);
		Assert.assertSame(validFlightRequest.getDateTime(), flightRequestRepo.findById(flightRequestid).getDateTime());
		Assert.assertEquals(RequestStatusEnum.CREATED, flightRequestRepo.findById(flightRequestid).getRequestStatus());
		Assert.assertEquals(customer.getId(), flightRequestRepo.findById(flightRequestid).getCustomer().getId());
	}
	
	@Test(expected = Exception.class)
	public void testFlightRequestJPAException() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(createAndSaveCustomer());
		// not valid data. Passengers can't be 0.  
		flightRequest.setAmountOfpassangers(0);
		// Try to save. Expect an Exception. 
		flightRequestRepo.persist(flightRequest);
	}

	@Override
	protected JpaFlightRequestRepo getRepository() {
		return flightRequestRepo;
	}

	@Override
	protected FlightRequest getEntity1() {
		FlightRequest validFlightRequest = TestFixture.getValidFlightRequest();
		customerRepo.persist(validFlightRequest.getCustomer());
		return validFlightRequest;
	}

	@Override
	protected FlightRequest getEntity2() {
		FlightRequest validFlightRequest = TestFixture.getValidFlightRequest();
		customerRepo.persist(validFlightRequest.getCustomer());
		return validFlightRequest;
	}
}
