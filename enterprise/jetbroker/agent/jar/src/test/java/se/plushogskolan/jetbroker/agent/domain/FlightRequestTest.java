package se.plushogskolan.jetbroker.agent.domain;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Unit test for Flight request.class  
 * @author PaulsMacbookPro
 */
public class FlightRequestTest {

	@Test
	public void testNewFlightRequestStatus() {
		FlightRequest flightRequest = new FlightRequest();
		Assert.assertEquals(RequestStatusEnum.CREATED, flightRequest.getRequestStatus());
	}
	
}
