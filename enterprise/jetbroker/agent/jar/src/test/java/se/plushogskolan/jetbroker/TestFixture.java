package se.plushogskolan.jetbroker;

import java.util.logging.Logger;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;

/**
 * Tempelate constructors for Costumer.class. 
 * @author PaulsMacbookPro
 */
public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.getName());

	/**
	 * Create a new customer for testing. All params in constructor.
	 * @return a ValidCustomer. 
	 */
	public static Customer getValidCustomer(long id, String firstName, String lastname, String email, String company) {
		Customer customer = new Customer();
		customer.setId(id);
		customer.setFirstName(firstName);
		customer.setLastName(lastname);
		customer.setEmail(email);
		customer.setCompany(company);
		return customer;
	}

	/**
	 * Create a new customer for testing. Only "Id" in constructor.
	 * @return a ValidCustomer. 
	 */
	public static Customer getValidCustomer(long id) {
		return getValidCustomer(id, "Customer name "+id, "Customer lastname", "poplabbet@gmail.com", "company");
	}
	/**
	 * Create a new customer for testing. No params needed in constructor.
	 * @return a ValidCustomer.
	 */
	public static Customer getValidCustomer() {
		return getValidCustomer(0, "Customer name ", "Customer lastname", "poplabbet@gmail.com", "company");
	}

	/**
	 * Create a new flight request for testing. No params needed in constructor.
	 * OBS! Not suitable for integration tests.  
	 * @return a ValidFlightRequest (With an unsaved customer).
	 */
	public static FlightRequest getValidFlightRequest() {
		return new FlightRequest(getValidCustomer(), 100, "ABC", "CDE", DateTime.now());
	}
	/**
	 * Create a new flight request for testing. A Customer object needed in constructor.
	 * @return a ValidFlightRequest.
	 */
	public static FlightRequest getValidFlightRequest(Customer customer) {
		return new FlightRequest(customer, 100, "ABC", "CDE", DateTime.now());
	}

	/**
	 * @return a virtual "war" file (Webarchive) for integration testing.
	 */
	public static Archive<?> createIntegrationTestArchive() {

		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "agent_test.war").addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}
}
