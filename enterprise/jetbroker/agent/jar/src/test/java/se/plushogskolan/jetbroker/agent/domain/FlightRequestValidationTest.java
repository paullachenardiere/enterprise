package se.plushogskolan.jetbroker.agent.domain;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.PropertyValidator;


/**
 * Integration test for Flight request.class 
 * Tests basic functionality and validation.
 * @author PaulsMacbookPro
 */
public class FlightRequestValidationTest {

	FlightRequest flightRequest;
	PropertyValidator<FlightRequest> validator;

	@Before
	public void init() {
		validator = new PropertyValidator<FlightRequest>();
		flightRequest = new FlightRequest();
	}

	@Test
	public void customerNotNull() {
		Set<ConstraintViolation<FlightRequest>> violations = validator.getValidator().validate(flightRequest);
		validator.assertPropertyIsInvalid("customer", violations);
	}

	@Test
	public void passengerMax500() {
		flightRequest.setAmountOfpassangers(501);
		Set<ConstraintViolation<FlightRequest>> violations = validator.getValidator().validate(flightRequest);
		validator.assertPropertyIsInvalid("amountOfpassangers", violations);
	}

	@Test
	public void passengerMin1() {
		flightRequest.setAmountOfpassangers(0);
		Set<ConstraintViolation<FlightRequest>> violations = validator.getValidator().validate(flightRequest);
		validator.assertPropertyIsInvalid("amountOfpassangers", violations);
	}
}
