package se.plushogskolan.jetbroker.agent.mvc;

import java.util.Locale;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

/**
 * Controller for creating and editing customers.
 * @author PaulsMacbookPro
 */
@Controller
@RequestMapping("/editCustomer")
public class EditCustomerController {

	private static Logger log = Logger.getLogger(EditCustomerController.class.getName());

	@Autowired
	MessageSource messageSource;

	@Inject
	CustomerService customerService;

	//Get (Gets all customers)
	@RequestMapping( value = "/{id}", method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {
		ModelAndView modelAndView = new ModelAndView("editCustomer");

		CustomerBean customerBean = new CustomerBean();

		if(id > 0){
			Customer customer = getCustomerService().getCustomer(id);
			customerBean.copyCustomerValuesToBean(customer);
			log.fine("Edit customer = "+ customer.getFirstName() + " " + customer.getLastName());
		} else {
			log.fine("New Customer");
		}

		modelAndView.addObject("customerBean", customerBean);

		return modelAndView; 
	}
	//Post (Save or Update customer) 
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid CustomerBean customerBean, BindingResult errors, Locale locale) {

		// Just for testing messageSource. 
		if(!errors.hasErrors() && customerBean.getFirstName().contentEquals("i18n")) {
			log.fine(getMessageSource().getMessage("i18n.test.message", null, locale)); }
		// Just for testing messageSource. 

		if(errors.hasErrors()) {
			log.fine("CustomerBean has errors = "+errors.toString());
			ModelAndView modelAndView = new ModelAndView("editCustomer");
			modelAndView.addObject("customerBean", customerBean);
			return modelAndView;
		} 


		if(customerBean.getId() > 0) {
			Customer customer = getCustomerService().getCustomer(customerBean.getId());
			customerBean.copyBeanValuesToCustomer(customer);
			getCustomerService().updateCustomer(customer);
			log.fine("Updated customer = "+ customer.getFirstName() + " " + customer.getLastName());
		} else {
			Customer customer = new Customer();
			customerBean.copyBeanValuesToCustomer(customer);
			getCustomerService().createCustomer(customer);
			log.fine("Created customer = "+ customer.getFirstName() + " " + customer.getLastName());
		}

		return new ModelAndView("redirect:/index.html");
	}

	//Delete a customer 
	@RequestMapping( value = "/{id}/delete", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable long id) {

		System.out.println("id= "+id);
		Customer customer = getCustomerService().getCustomer(id);
		System.out.println(customer);
		getCustomerService().deleteCustomer(customer.getId());
		log.fine("Deleted customer = "+ customer.getFirstName() + " " + customer.getLastName());
		return new ModelAndView("redirect:/index.html");
	}

	public CustomerService getCustomerService() {
		return customerService;
	}
	public MessageSource getMessageSource() {
		return messageSource;
	}
}