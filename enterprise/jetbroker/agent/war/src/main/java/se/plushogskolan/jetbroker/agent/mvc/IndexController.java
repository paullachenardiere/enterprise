package se.plushogskolan.jetbroker.agent.mvc;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.service.AirportService;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

/**
 * Controller for the main page of the Jetbroker system.
 * Handles the display of customers and flight requests.
 * @author PaulsMacbookPro
 */
@Controller
public class IndexController {

	@Inject
	CustomerService customerService;
	@Inject
	FlightRequestService flightRequestService;
	@Inject
	AirportService airportService;


	@RequestMapping("/index.html")
	public ModelAndView index() {

		ModelAndView modelAndView = new ModelAndView("index");
		List<Customer> allCustomers = getCustomerService().getAllCustomers();
		List<FlightRequest> allFlightRequests = getFlightRequestService().getAllFlightRequests();

		HashMap<String, Airport> allAirports = new HashMap<String, Airport>();
		for(Airport a : getAirportService().getAllAirports()) {
			allAirports.put(a.getCode(), a);
		}

		ArrayList<FlightRequest> createdFlightRequests = new ArrayList<FlightRequest>();
		ArrayList<FlightRequest> offerReceivedFlightRequests = new ArrayList<FlightRequest>();
		ArrayList<FlightRequest> recectedFlightRequests = new ArrayList<FlightRequest>();
		for(FlightRequest fr : allFlightRequests) {
			switch (fr.getRequestStatus()) {
			case CREATED:
			case REQUEST_CONFIRMED:
				createdFlightRequests.add(fr);
				break;
			case OFFER_RECEIVED:
				offerReceivedFlightRequests.add(fr);
				break;
			case REJECTED:
				recectedFlightRequests.add(fr);
				break;
			}
		}

		modelAndView.addObject("createdFlightRequests", createdFlightRequests);
		modelAndView.addObject("offerReceivedFlightRequests", offerReceivedFlightRequests);
		modelAndView.addObject("recectedFlightRequests", recectedFlightRequests);
		modelAndView.addObject("allCustomers", allCustomers);
		modelAndView.addObject("allAirports", allAirports);

		return modelAndView; 
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public AirportService getAirportService() {
		return airportService;
	}
}
