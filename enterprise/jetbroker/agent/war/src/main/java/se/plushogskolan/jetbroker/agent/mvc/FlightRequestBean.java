package se.plushogskolan.jetbroker.agent.mvc;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirport;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;

/**
 * Bean for the FlightRequest.class object. Includes Validation.
 * @author PaulsMacbookPro
 */
@ArrivalAndDepartureAirport
public class FlightRequestBean implements ArrivalAndDepartureAirportHolder{

	private long id;
	
	@NotNull(message = "{validation.flightRequest.customer.missing}")
	private Long customerId;
	
	@NotNull
	@Range(min = 1, max = 500)
	private Integer amountOfpassangers; 
	
	@NotBlank
	private String departureAirportCode;
	
	@NotBlank
	private String arrivalAirportCode;
	
	@NotNull
	@DateTimeFormat(pattern = "YYYY-MM-dd")
	private DateTime date;
	
	@NotNull
	@Range(min = 0, max = 23)
	private Integer hour;
	
	@NotNull
	@Range(min = 0, max = 59)
	private Integer minute;
	
	
	public void copyBeanValuesToFlightRequest (FlightRequest flightRequest, Customer customer)  {
		MutableDateTime mutableDate = getDate().toMutableDateTime(); 
		mutableDate.setHourOfDay(getHour());
		mutableDate.setMinuteOfHour(getMinute());
		
		flightRequest.setId(getId());
		flightRequest.setCustomer(customer);
		flightRequest.setAmountOfpassangers(getAmountOfpassangers());
		flightRequest.setArrivalAirportCode(getArrivalAirportCode());
		flightRequest.setDepartureAirportCode(getDepartureAirportCode());
		flightRequest.setDateTime(mutableDate.toDateTime());
	}

	public void copyFlightRequestValuesToBean (FlightRequest flightRequest)  {
		setId(flightRequest.getId());
		setCustomerId(flightRequest.getCustomer().getId());
		setAmountOfpassangers(flightRequest.getAmountOfpassangers());
		setDepartureAirportCode(flightRequest.getDepartureAirportCode());
		setArrivalAirportCode(flightRequest.getArrivalAirportCode());
		setDate(flightRequest.getDateTime());
		setHour(flightRequest.getDateTime().getHourOfDay());
		setMinute(flightRequest.getDateTime().getMinuteOfHour());
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Integer getAmountOfpassangers() {
		return amountOfpassangers;
	}

	public void setAmountOfpassangers(Integer amountOfpassangers) {
		this.amountOfpassangers = amountOfpassangers;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode.trim();
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode.trim();
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public Integer getMinute() {
		return minute;
	}

	public void setMinute(Integer minute) {
		this.minute = minute;
	}
}