package se.plushogskolan.jetbroker.agent.mvc;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.service.AirportService;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

/**
 * Controller for creating and editing flight requests. 
 * @author PaulsMacbookPro
 *
 */
@Controller
@RequestMapping("editFlightRequest")
public class EditFlightRequestController {

	private static Logger log = Logger.getLogger(EditFlightRequestController.class.getName());

	@Inject
	FlightRequestService flightRequestService;
	@Inject
	CustomerService customerService;
	@Inject
	AirportService airportService;


	//Get
	@RequestMapping( value = "/{id}", method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id){
		ModelAndView modelAndView = new ModelAndView("editFlightRequest");
		log.fine("Flight request controller (GET) called");

		FlightRequestBean flightRequestBean = new FlightRequestBean();

		if(id > 0) {
			FlightRequest flightRequest = getFlightRequestService().getFlightRequest(id);
			flightRequestBean.copyFlightRequestValuesToBean(flightRequest);
		}

		modelAndView.addObject("flightRequestBean", flightRequestBean);
		// Fill list
		fillFlightRequestFormWithValues(modelAndView);
		return modelAndView; 
	}


	@RequestMapping( value = "/{id}", method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid FlightRequestBean flightRequestBean, BindingResult errors) {
		log.fine("Flight request controller (POST) called");
		//Validation
		if(errors.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("editFlightRequest");
			modelAndView.addObject("flightRequestBean", flightRequestBean);
			// refill the list
			fillFlightRequestFormWithValues(modelAndView);
			return modelAndView;
		}

		Customer customer = getCustomerService().getCustomer(flightRequestBean.getCustomerId());

		//Existing flight request
		if(flightRequestBean.getId() > 0) {
			FlightRequest flightRequest = getFlightRequestService().getFlightRequest(flightRequestBean.getId());
			flightRequestBean.copyBeanValuesToFlightRequest(flightRequest, customer);
			getFlightRequestService().updateFlightRequest(flightRequest);
		}
		else{
			// New flight request
			FlightRequest flightRequest = new FlightRequest();
			flightRequestBean.copyBeanValuesToFlightRequest(flightRequest, customer);
			getFlightRequestService().createFlightRequest(flightRequest);
		}

		return new ModelAndView("redirect:/index.html");
	}
	// Helper method to fill the form with values.
	public void fillFlightRequestFormWithValues (ModelAndView modelAndView) {
		List<Customer> allCustomers = getCustomerService().getAllCustomers();
		modelAndView.addObject("allCustomers", allCustomers);
		List<Airport> allAirports = getAirportService().getAllAirports();
		modelAndView.addObject("allAirports", allAirports);
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}
	public CustomerService getCustomerService() {
		return customerService;
	}
	public AirportService getAirportService() {
		return airportService;
	}

	

}