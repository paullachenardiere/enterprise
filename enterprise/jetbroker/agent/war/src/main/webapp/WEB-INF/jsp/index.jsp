<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

<style type="text/css">
</style>

</head>
<body>

	<jsp:include page="header.jsp" />

	<br>
	<img class="floatLeft" src="<%=request.getContextPath()%>/images/flightrequest.png">
	<h2 class="underline"><spring:message code="index.flightRequest"/></h2>
	
	<br>
	<a class="button" class="link" class="floatLeft"
		href="<%=request.getContextPath()%>/editFlightRequest/0.html">
		<spring:message code="index.createNewFlightRequest"/></a>
	<br>
	<br>

	<h3><spring:message code="index.flightRequestWaiting"/></h3>
	<table class="dataTable">
		<tr>
			<th><spring:message code="global.id"/></th>
			<th><spring:message code="flightRequest.status"/></th>
			<th><spring:message code="flightRequest.departureAirport"/></th>
			<th><spring:message code="flightRequest.arrivalAirport"/></th>
			<th><spring:message code="flightRequest.date"/></th>
			<th><spring:message code="flightRequest.customer"/></th>
			<th><spring:message code="flightRequest.passengers"/></th>
			<th><spring:message code="global.edit"/></th>
		</tr>

		<c:if test="${empty createdFlightRequests}">
                <tr><td colspan="8"><spring:message code="index.noFlightRequest"/></td></tr>
            </c:if>		

		<c:forEach items="${createdFlightRequests}" var="flightRequest">
			<tr>
				<td>${flightRequest.id}</td>
				<td>${flightRequest.requestStatus.name}</td>
				<td>${allAirports[flightRequest.departureAirportCode].niceName}</td>
				<td>${allAirports[flightRequest.arrivalAirportCode].niceName}</td>
				<td>${flightRequest.dateTimeNiceName}</td>
				<td>${flightRequest.customer.niceName}</td>
				<td>${flightRequest.amountOfpassangers}</td>
				<td><a
					href="<%=request.getContextPath()%>/editFlightRequest/${flightRequest.id}.html">
						<img src="<%=request.getContextPath()%>/images/edit.png">
				</a></td>
			</tr>
		</c:forEach>
	</table>
	<br>
	<h3><spring:message code="index.flightRequestOffer"/></h3>
	
	<table class="dataTable">
		<tr>
			<th><spring:message code="global.id"/></th>
			<th><spring:message code="flightRequest.status"/></th>
			<th><spring:message code="flightRequest.departureAirport"/></th>
			<th><spring:message code="flightRequest.arrivalAirport"/></th>
			<th><spring:message code="flightRequest.date"/></th>
			<th><spring:message code="flightRequest.customer"/></th>
			<th><spring:message code="flightRequest.passengers"/></th>
			<th><spring:message code="flightRequest.planeType"/></th>
			<th><spring:message code="flightRequest.offeredPrice"/></th>
		</tr>

		<c:if test="${empty offerReceivedFlightRequests}">
                <tr><td colspan="9"><spring:message code="index.noFlightRequest"/></td></tr>
            </c:if>

		<c:forEach items="${offerReceivedFlightRequests}" var="flightRequest">
			<tr>
				<td>${flightRequest.id}</td>
				<td>${flightRequest.requestStatus.name}</td>
				<td>${allAirports[flightRequest.departureAirportCode].niceName}</td>
				<td>${allAirports[flightRequest.arrivalAirportCode].niceName}</td>
				<td>${flightRequest.dateTimeNiceName}</td>
				<td>${flightRequest.customer.niceName}</td>
				<td>${flightRequest.amountOfpassangers}</td>
				<td>Planetype placeholder</td>
				<td>Price placeholder</td>
			</tr>
		</c:forEach>
	</table>

	<br>

	<h3><spring:message code="index.flightRequestRejected"/></h3>
	<table class="dataTable">
		<tr>
			<th><spring:message code="global.id"/></th>
			<th><spring:message code="flightRequest.status"/></th>
			<th><spring:message code="flightRequest.departureAirport"/></th>
			<th><spring:message code="flightRequest.arrivalAirport"/></th>
			<th><spring:message code="flightRequest.date"/></th>
			<th><spring:message code="flightRequest.customer"/></th>
			<th><spring:message code="flightRequest.passengers"/></th>
		</tr>

		<c:if test="${empty recectedFlightRequests}">
                <tr><td colspan="7"><spring:message code="index.noFlightRequest"/></td></tr>
            </c:if>

		<c:forEach items="${recectedFlightRequests}" var="flightRequest">
			<tr>
				<td>${flightRequest.id}</td>
				<td>${flightRequest.requestStatus.name}</td>
				<td>${allAirports[flightRequest.departureAirportCode].niceName}</td>
				<td>${allAirports[flightRequest.arrivalAirportCode].niceName}</td>
				<td>${flightRequest.dateTimeNiceName}</td>
				<td>${flightRequest.customer.niceName}</td>
				<td>${flightRequest.amountOfpassangers}</td>
			</tr>
		</c:forEach>
	</table>

	<br>
	<br>

	<img class="floatLeft" src="images/customer.png">
	<h2 class="underline"><spring:message code="customer.customers"/></h2>

	<br>
	<a class="button" class="link" class="floatLeft"
		href="<%=request.getContextPath()%>/editCustomer/0.html"><spring:message code="index.createNewCustomer"/></a>
	<br>
	<br>
	<table class="dataTable">
		<tr>
			<th><spring:message code="global.id"/></th>
			<th><spring:message code="customer.firstName"/></th>
			<th><spring:message code="customer.lastName"/></th>
			<th><spring:message code="customer.company"/></th>
			<th><spring:message code="global.edit"/></th>
			<th><spring:message code="global.delete"/></th>
		</tr>

		<c:forEach items="${allCustomers}" var="customer">
			<tr>
				<td>${customer.id}</td>
				<td>${customer.firstName}</td>
				<td>${customer.lastName}</td>
				<td>${customer.company}</td>

				<td><a
					href="<%=request.getContextPath()%>/editCustomer/${customer.id}.html">
						<img src="<%=request.getContextPath()%>/images/edit.png">
				</a></td>
				<td><a
					href="<%=request.getContextPath()%>/editCustomer/${customer.id}/delete.html">
						<img src="<%=request.getContextPath()%>/images/delete.png">
				</a></td>
			</tr>
		</c:forEach>
	</table>
	<br>
</body>

</html>