<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

<style type="text/css">
</style>

</head>
<body>

	<jsp:include page="header.jsp" />
	<br>
	<img class="floatLeft"
		src="<%=request.getContextPath()%>/images/customer.png">
	<h2 class="underline">
		<c:choose>
			<c:when test="${customerBean.id > 0}">
				<spring:message code="editCustomer.editCustomer" />
			</c:when>
			<c:otherwise>
				<spring:message code="editCustomer.newCustomer" />
			</c:otherwise>
		</c:choose>
	</h2>


	<form:form commandName="customerBean">

		<table class="floatLeft" class="formTable">
			<tr>
				<th><spring:message code="customer.firstName" />:</th>
				<td><form:input type="text" path="firstName"
						value="${customerBean.firstName}" /></td>
				<td><form:errors path="firstName" cssClass="error" /></td>
			</tr>
			<tr>
				<th><spring:message code="customer.lastName" />:</th>
				<td><form:input type="text" path="lastName"
						value="${customerBean.lastName}" /></td>
				<td><form:errors path="lastName" cssClass="error" /></td>
			</tr>
			<tr>
				<th><spring:message code="customer.email" />:</th>
				<td><form:input type="text" path="email"
						value="${customerBean.email}" /></td>
				<td><form:errors path="email" cssClass="error" /></td>
			</tr>
			<tr>
				<th><spring:message code="customer.company" />:</th>
				<td><form:input type="text" path="company"
						value="${customerBean.company}" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit"
					value="<spring:message code="global.submit"/>" /> <a
					href="<%=request.getContextPath()%>/index.html"><spring:message
							code="global.cancel" /></a></td>
			</tr>
		</table>

	</form:form>


</body>

</html>