<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<style type="text/css">
</style>

<script>
	$(function() {
		$("#datepicker").datepicker({
			dateFormat : "yy-mm-dd"
		});
	});
</script>

</head>

<body>

	<jsp:include page="header.jsp" />

	<br>
	<img class="floatLeft"
		src="<%=request.getContextPath()%>/images/flightrequest.png">

	<h2 class="underline">
		<c:choose>
			<c:when test="${flightRequestBean.id > 0}">
				<spring:message code="editFlightRequest.editFlightRequest" />
			</c:when>
			<c:otherwise>
				<spring:message code="editFlightRequest.addNewFlightRequest" />
			</c:otherwise>
		</c:choose>
	</h2>

	<form:form commandName="flightRequestBean">

		<table class="floatLeft" class="formTable">

			<tr>
				<th><spring:message code="flightRequest.customer"/></th>
				<td><form:select type="text" path="customerId">
						<form:option value="" label=""></form:option>
						<c:forEach items="${allCustomers}" var="customer">
							<form:option value="${customer.id}">
							${customer.niceName} - ${customer.company}</form:option>
						</c:forEach>
					</form:select></td>
				<td><form:errors path="customerId" cssClass="error" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightRequest.departureAirport"/></th>
				<td><form:select type="text" path="departureAirportCode">
						<form:option value=""></form:option>
						<c:forEach items="${allAirports}" var="airport">
							<form:option value="${airport.code}">
							${airport.niceName}</form:option>
						</c:forEach>
					</form:select></td>
				<td><form:errors path="" cssClass="error" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightRequest.arrivalAirport"/></th>
				<td><form:select type="text" path="arrivalAirportCode">
						<form:option value=""></form:option>
						<c:forEach items="${allAirports}" var="airport">
							<form:option value="${airport.code}">
							${airport.niceName}</form:option>
						</c:forEach>
					</form:select></td>
				<td><form:errors path="" cssClass="error" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightRequest.passengers"/></th>
				<td><form:input type="text" path="amountOfpassangers" /></td>
				<td><form:errors path="amountOfpassangers" cssClass="error" /></td>
			</tr>
			<tr>
				<th><spring:message code="editFlightRequest.departureDate"/></th>
				<td><form:input type="text" path="date" id="datepicker" /></td>
				<td><form:errors path="date" cssClass="error" /></td>
			</tr>
			<tr>
				<th><spring:message code="editFlightRequest.departureTime"/></th>
				<td><form:select path="hour">
						<form:option value="" label=""></form:option>
						<c:forEach var="hour" begin="0" end="23">
							<form:option value="${hour}">${hour}</form:option>
						</c:forEach>
					</form:select></td>
				<td><form:errors path="hour" cssClass="error" /></td>
			</tr>
			<tr>
				<th></th>
				<td><form:select path="minute">
						<form:option value="" label=""></form:option>
						<c:forEach var="minute" begin="0" end="59" step="5">
							<form:option value="${minute}">${minute}</form:option>
						</c:forEach>
					</form:select></td>
				<td><form:errors path="minute" cssClass="error" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="<spring:message code="global.submit"/>" /> <a
					href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"/></a></td>
			</tr>
		</table>

	</form:form>

</body>

</html>