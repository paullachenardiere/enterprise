Skapa en ny fil, <ditt repo>/enterprise/questions_jetbroker_setup.txt. Kopiera frågorna nedan in i den filen. Svara på frågorna nedan, skriv svaret efter varje fråga, och checka in filen. 

För att skapa filen i enterprise-mappen kan det vara lättare att göra det via utforskaren (dvs ej genom eclipse eftersom du inte har enterprisemappen som ett projekt). Checka in via kommand line eller SourceTree. 
---------------------------------------------------------------------------------------------------------------------

FRÅGA: Vad heter plugin:et som genererar war-filer?
SVAR: maven-war-plugin

FRÅGA: Vad heter plugin:et som genererar ear-filer?
SVAR: maven-ear-plugin

FRÅGA: Dependency:n Hibernate-validator har scope=provided, varför då?
SVAR: Därför att den ingår i JBoss 7. 

FRÅGA: För vilken javaversion kompilerar vi vår kod?
SVAR: Java 1.6

FRÅGA: Hur vet maven att vi använder EJB3? Vilken plugin i vilken pom-fil?
SVAR: Genom att vi använder plugin ”maven-ejb-plugin” och ”ejbVersion 3.1”. Det står i pom.xml i jee-parent. 

FRÅGA: Vilka två profiler har vi att välja på när vi vill köra ett integrationstest med Arquillian?
SVAR: ”jboss” och ”jboss-remote”.

FRÅGA: Vilka två artifakter kommer att ingå i agent-ear?
SVAR: ”agent-jar” och ”agent-war”
 
FRÅGA: Om vi vill deploya agentsystemet under localhost:8080/agent istället för localhost:8080/jetbroker-agent, var ändrar vi det?
SVAR: Man ändrar ”<contextRoot>/jetbroker-agent</contextRoot>” till ”<contextRoot>/agent</contextRoot>”. Detta görs i agent-ear pom.xml. Kör sedan: ”mvn clean install jboss-as:deploy” 

FRÅGA: Beskriv vad följande mavenkommandon gör, plus sortera dom i den ordning de utförs (googla)
- clean (utförs inte i någon ordning, separat kommando):
- compile:
- test:
- package:
- install:


SVAR: 
- clean: Tar bort tidigare kompilerad kod.  
- compile: Kompilerar projektets källkod.
- test: Testar källkoden med ett ramverk för enhetstester t.ex JUnit. Koden testas även om den inte är paketerad.
- package: Paketerar den redan kompilerade källkoden till exempelvis en körbar JAR.
- install: Installerar paketet (JAR,WAR…) i ditt lokala repo. Det kan sedan användas av andra lokala projekt.


FRÅGA: Vilken xml-fil genereras automatiskt i agent-ear/target när man kör “mvn package” på projektet. Testa! En bra idé är att göra en “mvn clean” först. 
SVAR: application.xml

FRÅGA: Om jag vill ladda databasen med exempeldata när applikationen startas upp, vilken fil skall jag använda då?
SVAR: import.sql (plane.jar - src/main/resources…)			

FRÅGA: Vilka värden kan jag använda för “hibernate.hbm2ddl.auto” i persistence.xml (googla). 
SVAR: validate, update, create, create-drop

FRÅGA: Hur vet Arquillian att vi har en egen konfigurationsfil (enterprisekurs.xml) som vi vill använda när vi kör integrationstester i JBoss?
SVAR: Det finns specificerat i arquillian.xml i respektive ”src/test/resources”.

FRÅGA: Om jag vill använda samma stylesheet som Plane-systemet, vad heter den css-filen och var ligger den?
SVAR: Filen heter ”common.css” och ligger i common-war/src/main/webapp/style

FRÅGA: Vilka heter de två felsidor (*.jsp) som finns förberedda i systemet?
SVAR: ”failedIntegrationException.jsp”, ”generalError.jsp”.

FRÅGA: Om jag vill aktivera annoteringen MockIntegration.java, i vilken fil gör jag det?
SVAR: I filen ”beans.xml”

FRÅGA: Vilken metod måste alla klasser implementera som implementerar interfacet IdHolder?
SVAR: Metoden ”long getId();”

FRÅGA: Om en repository implementerar BaseRepository, vilka metoder måste finnas då?
SVAR:   long persist(E entity);
	void remove(E entity);
	E findById(long id);
	void update(E entity);


FRÅGA: Om jag vill skriva mindre kod i mina repositories, vilken klass skall jag ärva från då?
SVAR: JpaRepository.java

FRÅGA: Om en klass är annoterad med @AirportCode (som är en klass vi har skapat), hur många tecken måste en flygplatskod ha för att vara giltig, och vilken klass kollar detta?
SVAR: Den måste vara 3 tecken lång. Klassen heter ”AirportCodeValidator”.

FRÅGA: Vilka två domänobjekt finns det i plane-jar? Sparas båda i databasen?
SVAR: ”Airport” och ”PlaneType”. Båda är entiteter och sparas i db.

FRÅGA: Om jag har en referens till AirportRepository, hur många metoder kan jag då anropa. Tänk på arv men ignorera metoder som finns i java.lang.Objekt. 
SVAR: Du kan anropa 7 st metoder.

FRÅGA: Vilken klass-annotering (annotering på själva klassdefinitionen, ej på metoderna) finns det på AirportService.java? Vilken klass-annotering finns det på klassen AirportServiceImpl?
SVAR: ”AirportService.java” har annoteringen ”@Local”. ”AirportServiceImpl” har annoteringen ”@Stateless”.	

FRÅGA: Vilket pattern använder sig JpaAirportRepositoryIntegrationTest av? Hint, kolla arvet. Försök förstå detta pattern. 
SVAR: Template pattern.

FRÅGA: Plane-war har en dependency till common-war. Vilken “type” har denna dependency i pom-filen?
SVAR: Den är av typen ”war”.

FRÅGA: Vilken version av spring-web och spring-mvc använder vi oss av i plane-war?
SVAR: 3.2.1.RELEASE






 


