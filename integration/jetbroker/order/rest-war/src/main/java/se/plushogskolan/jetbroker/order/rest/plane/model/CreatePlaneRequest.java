package se.plushogskolan.jetbroker.order.rest.plane.model;

import se.plushogskolan.jetbroker.order.domain.Plane;

public class CreatePlaneRequest {

	private String planeTypeCode;
	private String description;

	public CreatePlaneRequest () {}
	
	public Plane builtPlane() {
		Plane plane = new Plane();
		plane.setPlaneTypeCode(getPlaneTypeCode());
		plane.setDescription(getDescription());
		return plane;
	}
	
	
	
	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Plane [planeTypeCode=" + planeTypeCode
				+ ", description=" + description + "]";
	}

}
