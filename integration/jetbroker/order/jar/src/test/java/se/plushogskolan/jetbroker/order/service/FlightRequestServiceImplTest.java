package se.plushogskolan.jetbroker.order.service;

import javax.jms.JMSException;

import junit.framework.Assert;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {

	@Test
	public void handleIncomingFlightRequest() throws JMSException {
		
		// Setup domain objects
		Airport airport1 = new Airport("GBG", "Gothenburg [Mocked]", 57.697, 11.98);
		Airport airport2 = new Airport("STM", "Stockholm [Mocked]", 59.33, 18.06);
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(0);
		flightRequest.setArrivalAirportCode("GBG");
		flightRequest.setDepartureAirportCode("STM");
	
		// Facade Mock
		AgentIntegrationFacade facade = EasyMock.createMock(AgentIntegrationFacade.class);
		facade.sendFlightRequestConfirmation(EasyMock.isA(FlightRequestConfirmation.class));

		// Repo Mock
		FlightRequestRepository repo = EasyMock.createMock(FlightRequestRepository.class);
		Capture<FlightRequest> capture = new Capture<FlightRequest>();
		EasyMock.expect(repo.persist(EasyMock.capture(capture))).andReturn(1L);
		
		// PlaneService Mock
		PlaneService planeService = EasyMock.createMock(PlaneService.class);
		EasyMock.expect(planeService.getAirport("GBG")).andReturn(airport1);
		EasyMock.expect(planeService.getAirport("STM")).andReturn(airport2);
		
		// Setup service
		FlightRequestServiceImpl service = new FlightRequestServiceImpl();
		EasyMock.replay(repo, planeService, facade);
		service.setFlightRequestRepository(repo);
		service.setPlaneService(planeService);
		service.setAgentIntegrationFacade(facade);
		
		// Make call
		service.handleIncomingFlightRequest(flightRequest);

		// Verify and assert
		EasyMock.verify(repo, planeService, facade);

		FlightRequest flightRequestCaptured = capture.getValue();
		Assert.assertSame("Flight request status NEW",  FlightRequestStatus.NEW, flightRequestCaptured.getStatus());
		Assert.assertEquals(396, flightRequestCaptured.getDistanceKm() , 0);
	}
	
}
