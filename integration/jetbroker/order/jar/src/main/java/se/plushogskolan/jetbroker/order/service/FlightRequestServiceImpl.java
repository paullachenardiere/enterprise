package se.plushogskolan.jetbroker.order.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSException;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.map.HaversineDistance;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

/**
 * Service class for Flight request object.
 * Handle incoming flight requests and saves the object.
 * Sends a FlightRequestConfirmation back to "Agent" artifact. 
 * Calles FlightRequestRepository, PlaneService and AgentIntegrationFacade.
 * @author PaulsMacbookPro
 */
@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {
	
	private static Logger log = Logger.getLogger(FlightRequestServiceImpl.class.getName());

	@Inject
	private FlightRequestRepository flightRequestRepository;

	@Inject
	private PlaneService planeService;

	@Inject
	@Prod
	private AgentIntegrationFacade agentIntegrationFacade;
	
	/**
	 * Handle incoming flight requests, does some logic and saves the object.
     * Sends a FlightRequestConfirmation (thru the facade) back to the "Agent" artifact.
	 */
	@Override
	public FlightRequestConfirmation handleIncomingFlightRequest(FlightRequest request) {
		log.fine("ORDER handleIncomingFlightRequest() = " + request.toString());
		Airport airport = getPlaneService().getAirport(request.getDepartureAirportCode());
		Airport airport2 = getPlaneService().getAirport(request.getArrivalAirportCode());

		double distanceKm = HaversineDistance.getDistance(airport.getLatitude(), airport.getLongitude(),
				airport2.getLatitude(), airport2.getLongitude());
		request.setDistanceKm((int) distanceKm);
		request.setStatus(FlightRequestStatus.NEW);

		long confirmationID = getFlightRequestRepository().persist(request);
		log.fine("Saves incoming flight request in DB. ID = " + confirmationID);
		
		FlightRequestConfirmation flightRequestConfirmation = new FlightRequestConfirmation(request.getAgentRequestId(), confirmationID);
		try {
			getAgentIntegrationFacade().sendFlightRequestConfirmation(flightRequestConfirmation);
			log.fine("ORDER. Sending the flightRequestConfirmation to the facade = " + flightRequestConfirmation.toString());
		} catch (JMSException e) {
			log.severe("An exeption occurred while sending the flightRequestConfirmation. The process was cancelled");
			e.printStackTrace();
		}
		return flightRequestConfirmation;
	}

	@Override
	public void handleUpdatedOffer(long flightRequestId, Offer offer) {

		// Implement
	}

	@Override
	public void rejectFlightRequest(long id) {

		// Implement

	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		return getFlightRequestRepository().findById(id);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return getFlightRequestRepository().getAllFlightRequests();
	}

	@Override
	public void deleteFlightRequest(long id) {
		
		FlightRequest flightRequest = getFlightRequest(id);
		getFlightRequestRepository().remove(flightRequest);

	}

	public FlightRequestRepository getFlightRequestRepository() {
		return flightRequestRepository;
	}

	public void setFlightRequestRepository(FlightRequestRepository flightRequestRepository) {
		this.flightRequestRepository = flightRequestRepository;
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

	public AgentIntegrationFacade getAgentIntegrationFacade() {
		return agentIntegrationFacade;
	}

	public void setAgentIntegrationFacade(
			AgentIntegrationFacade agentIntegrationFacade) {
		this.agentIntegrationFacade = agentIntegrationFacade;
	}

}
