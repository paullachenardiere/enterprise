package se.plushogskolan.jetbroker.order.integration.agent;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;

/**
 * Facade implementation to Agent artifact.
 * Sends a flight request in a queue using JMS. 
 * "MapMessage" are used for the FlightRequestConfirmation values.  
 * @author PaulsMacbookPro
 */

@Prod
public class AgentIntegrationFacadeImpl implements AgentIntegrationFacade{

	@Inject
	Logger log;
	
	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory factory;
	@Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE)
	private Queue responseQueue;


	@Override
	public void sendFlightRequestConfirmation(FlightRequestConfirmation response) throws JMSException {
		log.fine("ORDER sendFlightRequestConfirmation()." + response.toString());
		QueueConnection connection = null;
		QueueSession session = null;
		MapMessage message; 
		QueueSender sender;

		try {
		
			connection = factory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			message = session.createMapMessage();
			message.setLong("agentId", response.getAgentRequestId());
			message.setLong("confirmationID", response.getOrderRequestId());
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_CONFIRMATION);
			sender = session.createSender(responseQueue);
			sender.send(message);
			log.fine("ORDER. Sending FlightRequestConfirmation back to Agent with JMS (Queue). "
					+ "FlightRequestConfirmation = " + message);
		
		} catch (JMSException e) {
			log.severe("Could'nt send FlightRequestConfirmation to Agent");
			e.printStackTrace();
		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
			log.fine("Closing connection and session");
		}
	}

	@Override
	public void sendUpdatedOfferMessage(FlightRequest flightRequest) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void sendFlightRequestRejectedMessage(FlightRequest flightRequest) {
		// TODO Auto-generated method stub
	}
}
