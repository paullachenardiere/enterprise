package se.plushogskolan.jetbroker.order.integration.agent;

import javax.jms.JMSException;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;

public interface AgentIntegrationFacade {

	void sendUpdatedOfferMessage(FlightRequest flightRequest);

	void sendFlightRequestRejectedMessage(FlightRequest flightRequest);

	void sendFlightRequestConfirmation(FlightRequestConfirmation response) throws JMSException;
}
