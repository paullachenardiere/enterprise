package se.plushogskolan.jetbroker.order.integration.plane.mdb;


import java.util.logging.Logger;

import javax.ejb.MessageDriven;
import javax.ejb.ActivationConfigProperty;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.order.service.PlaneService;

/**
 * MDB recives a JMS message from the Plane artifact.
 * This mdb handles updates on the fuel price.     
 * @author PaulsMacbookPro
 */

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
				propertyName="destinationType",
				propertyValue="javax.jms.Topic"),
				@ActivationConfigProperty(
						propertyName="destination",
						propertyValue="java:jboss/exported/jms/topic/planeBroadcastTopic"),
						@ActivationConfigProperty(
								propertyName="messageSelector",
								propertyValue="messageType = '" + JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED + "'")
})
public class FuelPriceMDB extends AbstractMDB implements MessageListener {

	@Inject
	Logger log;

	@Inject
	PlaneService planeService;

	@Override
	public void onMessage(Message message) {
		log.fine("ORDER FuelPriceMDB onMessage");
		double newFuelPrice;
		try {
			newFuelPrice = message.getDoubleProperty("fuelPrice");
			planeService.updateFuelCostPerLiter(newFuelPrice);
			log.fine("Receiving new fuel price. Updating fuel price to; " + newFuelPrice);
		} catch (JMSException e) {
			log.fine("Receiving new fuel price. Something went wrong. Could'nt update fuel price.");
			e.printStackTrace();
		}
	}
}