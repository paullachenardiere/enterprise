package se.plushogskolan.jetbroker.order.integration.plane.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;

/**
 * MDB recives a JMS message from the Agent artifact.
 * This mdb handles an incoming flight request from agent.     
 * @author PaulsMacbookPro
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
		propertyName="destinationType",
		propertyValue="javax.jms.Queue"),
		@ActivationConfigProperty(
		propertyName="destination",
		propertyValue=JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST),
		@ActivationConfigProperty(
		propertyName="messageSelector",
		propertyValue="messageType = '" + JmsConstants.MSGTYPE_FLIGHTREQUEST_REQUEST + "'")
		})
public class FlightRequestMDB extends AbstractMDB implements MessageListener {
	
	@Inject
	Logger log;
	
	@Inject
	FlightRequestService flightRequestService;
	
	@Override
	public void onMessage(Message mess) {
		log.fine("ORDER FlightRequestMDB onMessage");
		FlightRequest flightRequest = new FlightRequest();
		MapMessage message = (MapMessage) mess;
		
		try {
			//Pair the message data to the new object
			flightRequest.setNoOfPassengers(Integer.valueOf(message.getInt("noOfPassengers")));
			flightRequest.setAgentRequestId(message.getLong("agentRequestId"));
			flightRequest.setDepartureAirportCode(message.getString("departureAirportCode"));
			flightRequest.setArrivalAirportCode(message.getString("arrivalAirportCode"));
			flightRequest.setDepartureTime(DateTime.parse(message.getString("departureTime")));
		
			// Sends the flight request to service to logic. 
			log.fine("Reciving new FlightRequest form Agent with JMS");
			flightRequestService.handleIncomingFlightRequest(flightRequest);
			
			
		} catch (JMSException e) {
			log.fine("Failed while reciving a new FlightRequest from Agent. An exception was thrown ");
			e.printStackTrace();
		}
		
	}
}
