package se.plushogskolan.jetbroker.order.integration.plane;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsAirportArray;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsPlaneType;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsPlaneTypeArray;
/**
 * An integration facade to "PLANE". Plane uses JAX-WS (Web Service) to send data.
 * This class uses WSDL to generate classes that's needed.	
 *
 * @Return  List<Airport> , List<PlaneType>
 * @author PaulsMacbookPro
 */
@Prod
public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade {

	@Inject
	Logger log;
	
	@Override
	public List<Airport> getAllAirports() {
	
		PlaneWebServiceImplService service = new PlaneWebServiceImplService();	
		PlaneWebService planeWebServicePort = service.getPlaneWebServicePort();
		
		WsAirportArray airports = planeWebServicePort.getAirports();
		List<WsAirport> airportsList = airports.getItem();
		List<Airport> allAirports = new ArrayList<>();
		for(WsAirport a : airportsList) {
			Airport airport = new Airport();
			airport.setCode(a.getCode());
			airport.setLatitude(a.getLatitude());
			airport.setLongitude(a.getLongitude());
			airport.setName(a.getName());
			allAirports.add(airport);
		}
		return allAirports;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		PlaneWebServiceImplService service = new PlaneWebServiceImplService();	
		PlaneWebService planeWebServicePort = service.getPlaneWebServicePort();
		
		WsPlaneTypeArray planeTypes = planeWebServicePort.getPlaneTypes();
		List<WsPlaneType> planeTypesList = planeTypes.getItem();
		List<PlaneType> allPlaneTypes = new ArrayList<>();
		for(WsPlaneType p : planeTypesList) {
			PlaneType planeType = new PlaneType();
			planeType.setCode(p.getCode());
			planeType.setFuelConsumptionPerKm(p.getFuelConsumptionPerKm());
			planeType.setMaxNoOfPassengers(p.getMaxNoOfPassengers());
			planeType.setMaxRangeKm(p.getMaxRangeKm());
			planeType.setMaxSpeedKmH(p.getMaxSpeedKmH());
			planeType.setName(p.getName());
			allPlaneTypes.add(planeType);
		}
		return allPlaneTypes;
	}

}
