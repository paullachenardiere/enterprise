package se.plushogskolan.jetbroker.agent.rest.flightrequest.model;

public class CreateFlightRequestRequest {
	private long customerId;
	private int noOfPassengers;
	private String departureAirportCode;
	private String arrivalAirportCode;
	private String date;

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public int getNoOfPassengers() {
		return noOfPassengers;
	}

	public void setNoOfPassengers(int noOfPassengers) {
		this.noOfPassengers = noOfPassengers;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "CreateFlightRequestRequest [customerId=" + customerId + ", noOfPassengers=" + noOfPassengers
				+ ", departureAirportCode=" + departureAirportCode + ", arrivalAirportCode=" + arrivalAirportCode
				+ ", date=" + date + "]";
	}

}
