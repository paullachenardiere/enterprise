package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import se.plushogskolan.jee.utils.xml.XmlUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

public class DomBuilder {

	public static String buildXmlUsingDom(FlightRequest fr) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();
		Element flightrequest = doc.createElement("flightrequest");
		flightrequest.setAttribute("id", String.valueOf(fr.getId()));
		doc.appendChild(flightrequest);
		flightrequest.appendChild(getFlightrequest(fr.getArrivalAirportCode(), fr.getDepartureAirportCode(),
				String.valueOf(fr.getNoOfPassengers()), fr.getRequestStatus().toString(), doc) );
		
		return XmlUtil.convertXmlDocumentToString(doc);
	}
	
	protected static Node getFlightrequest(String arrivalAirport, String departureAirport,
			String passengers, String requestStatusString, Document doc) {
				
		Element element = doc.createElement("flightrequest");
		
		Element arrivalAirportCode = doc.createElement("arrivalAirportCode");
		Element departureAirportCode = doc.createElement("departureAirportCode");
		Element noOfPassengers = doc.createElement("noOfPassengers");
		Element status = doc.createElement("status");
		
		element.appendChild(departureAirportCode);
		element.appendChild(arrivalAirportCode);	
		element.appendChild(noOfPassengers);
		element.appendChild(status);
		
		arrivalAirportCode.setTextContent(arrivalAirport);
		departureAirportCode.setTextContent(departureAirport);
		noOfPassengers.setTextContent(passengers);
		status.setTextContent(requestStatusString);
		
		return element;
		
	}


}
