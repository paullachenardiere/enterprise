package se.plushogskolan.jetbroker.agent.rest.customer.model;

import se.plushogskolan.jetbroker.agent.domain.Customer;

public class CreateCustomerRequest {

	private String firstName;
	private String lastName;
	private String email;
	private String company;

	public CreateCustomerRequest() {
	}

	public Customer BuildCustomer() {
		Customer customer = new Customer();
		customer.setFirstName(getFirstName());
		customer.setLastName(getLastName());
		customer.setEmail(getEmail());
		customer.setCompany(getCompany());
		return customer;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {

		return getFirstName() + " " + getLastName();
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "CreateCustomerRequest [firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", company=" + company + "]";
	}

}

