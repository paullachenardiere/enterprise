package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import se.plushogskolan.jetbroker.agent.rest.exercises.xml.model.BoardingCard;

public class JaxbTransformer {

	public static String transformToXml(BoardingCard boardingCard) throws Exception {

		JAXBContext context = JAXBContext.newInstance(BoardingCard.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		StringWriter stringOutput = new StringWriter();
		marshaller.marshal(boardingCard, stringOutput);
		
		System.out.println("stringOutput = "+stringOutput.toString());  
		return stringOutput.toString();

	}

}
