package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XsltTransformer {
	
	public static String transformUsingXslt(String xmlFile, String xlsFile) throws Exception {

		InputStream xml = XsltTransformer.class.getResourceAsStream(xmlFile);
		InputStream xls = XsltTransformer.class.getResourceAsStream(xlsFile);
		
		StreamSource xmlStream = new StreamSource(xml);
		StreamSource xlsStream = new StreamSource(xls);
		
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer xslTransformer = factory.newTransformer(xlsStream);
		
		StringWriter stringOutput = new StringWriter();
		StreamResult result = new StreamResult(stringOutput);
		xslTransformer.transform(xmlStream, result);
		
		return stringOutput.toString();
	}
}
