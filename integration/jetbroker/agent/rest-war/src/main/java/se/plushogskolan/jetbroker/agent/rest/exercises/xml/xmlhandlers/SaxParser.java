package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.InputStream;
import java.util.logging.Logger;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;

public class SaxParser {
	
	private static final Logger log = Logger.getLogger(SaxParser.class.getName());
	
	public static FlightRequest readFlightRequestFromXmlUsingSax(String file) throws Exception {

		
		
		log.fine("Flight Request String: " + file);
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(true);

		SAXParser parser = factory.newSAXParser();
		InputStream in = SaxParser.class.getResourceAsStream(file);
		
		
		Handler handler = new Handler();
		parser.parse(in, handler);
		return handler.getFlightRequest();
	}

	static class Handler extends DefaultHandler {
		
		private FlightRequest flightRequest;
		private String currentValue;
		
		public FlightRequest getFlightRequest() {
			return flightRequest;
		}
		
		@Override
		public void startDocument() throws SAXException {
			flightRequest = new FlightRequest();
		}
		
		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			
			   if (qName.equals("flightrequest")) {
	                flightRequest.setId(Long.parseLong(attributes.getValue("id")));
	            }
		}	

		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {
			currentValue = new String(ch, start, length);
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {

			if(qName.equalsIgnoreCase("id")) {
				flightRequest.setId(Long.valueOf(currentValue));
			}
			if(qName.equalsIgnoreCase("departureAirportCode")) {
				flightRequest.setDepartureAirportCode(currentValue);
			}
			if(qName.equalsIgnoreCase("arrivalAirportCode")) {
				flightRequest.setArrivalAirportCode(currentValue);
			}
			if(qName.equalsIgnoreCase("noOfPassengers")) {
				flightRequest.setNoOfPassengers(Integer.valueOf(currentValue));
			}
			if(qName.equalsIgnoreCase("status")) {
				flightRequest.setRequestStatus(FlightRequestStatus.valueOf(currentValue));
			}
		
			log.fine("Flight Request End element: " + flightRequest.toString());
		}


		


	}

}
