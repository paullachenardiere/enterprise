package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import static org.junit.Assert.*;

import org.junit.Test;

public class XsltTransformerTest {

	@Test
	public void testTransformUsingXslt() throws Exception {

		 String html =
		 XsltTransformer.transformUsingXslt("/flightRequest.xml",
		 "/flightRequest.xsl");
		 assertTrue("ID", html.contains("<h1>Flight request - 5</h1>"));
		 assertTrue("1", html.contains("<p>Departure airport code: GBG</p>"));
		 assertTrue("2", html.contains("<p>arrivalAirportCode: STM</p>"));
		 assertTrue("3", html.contains("<p>No of passengers: 12</p>"));
		 assertTrue("4", html.contains("<p>Status: CREATED</p>"));

	}

}
