package se.plushogskolan.jetbroker.agent.integration.plane.mdb;


import java.util.logging.Logger;

import javax.ejb.MessageDriven;
import javax.ejb.ActivationConfigProperty;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.services.AirPortService;

/**
* MDB recives a JMS message from the Plane artifact.
* This mdb handles updates on airports.     
* @author PaulsMacbookPro
*/

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
		propertyName="destinationType",
		propertyValue="javax.jms.Topic"),
		@ActivationConfigProperty(
		propertyName="destination",
		propertyValue="java:jboss/exported/jms/topic/planeBroadcastTopic"),
		@ActivationConfigProperty(
		propertyName="messageSelector",
		propertyValue="messageType = '" + JmsConstants.MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED + "'")
		})
public class AirportChangedMDB extends AbstractMDB implements MessageListener {
	
	@Inject
	Logger log;
	
	@Inject
	AirPortService airPortService;
	
	@Override
	public void onMessage(Message message) {
		log.fine("AGENT on message. Agent. MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED");
		log.fine("Calling airPortService.handleAirportsChangedEvent();");
		airPortService.handleAirportsChangedEvent();
	
	}


}
