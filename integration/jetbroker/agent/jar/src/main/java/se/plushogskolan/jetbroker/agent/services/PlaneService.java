package se.plushogskolan.jetbroker.agent.services;

import java.net.MalformedURLException;
import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;

@Local
public interface PlaneService {

	List<PlaneType> getAllPlaneTypes() throws MalformedURLException;

	PlaneType getPlaneType(String code) throws MalformedURLException;

	void handlePlaneTypesChangedEvent();

}
