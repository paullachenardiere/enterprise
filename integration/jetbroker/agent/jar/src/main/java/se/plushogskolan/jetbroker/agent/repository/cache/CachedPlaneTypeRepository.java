package se.plushogskolan.jetbroker.agent.repository.cache;

import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

/**
 * Takes care of getting all plane types from "PLANE". Then saves the data into a cache.
 * Contains logic if plane types exists in cache or not.   
 * Clears the cache if any changes are made on plane types that is saves the "PLANE" artifact.  
 * @author PaulsMacbookPro
 */
@Stateless
public class CachedPlaneTypeRepository implements PlaneTypeRepository {
	@Inject
	Logger log;

	@Prod
	@Inject
	private PlaneFacade planeFacade;

	private static final String All_PLANETYPES = "allPlaneTypes";
	private static final String PLANETYPE_CACHE = "planeTypeCache";

	// Gets the cache instance.
	private static Cache planeTypeCache = CacheManager.getInstance().getCache(PLANETYPE_CACHE);



	@SuppressWarnings("unchecked")
	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("Get all Plane Types.");

		boolean keyInCache = planeTypeCache.isKeyInCache(All_PLANETYPES);

		if(!keyInCache) {
			log.fine("Plane types does not exists in cache");
			log.fine("Plane types cache. Number of elements = "+planeTypeCache.getKeys().size());
			List<PlaneType> allPlaneTypes = planeFacade.getAllPlaneTypes();
			log.fine("Saves plane Types in cache");
			planeTypeCache.put(new Element(All_PLANETYPES, allPlaneTypes));
			log.fine("Plane types cache. Number of elements = "+planeTypeCache.getKeys().size());
			return allPlaneTypes;
		} 
		log.fine("Plane Types exists in cache!");
		return (List<PlaneType>) planeTypeCache.get("allPlaneTypes").getObjectValue();
	}

	@Override
	public PlaneType getPlaneType(String code) throws MalformedURLException {
		log.fine("get Plane Type, code="+ code );

		if(planeTypeCache.isKeyInCache(code)) {
			log.fine("Plane Type exists in cache with it's own key");
			return (PlaneType) planeTypeCache.get(code).getObjectValue();
		} 
		log.fine("Plane Type doesn't exists in cache with it's own key");
		PlaneType planeType = findPlaneTypeWithCode(code);
		log.fine("adding plane Type in cache. Code = "+ planeType.getCode());
		planeTypeCache.put(new Element(planeType.getCode(), planeType));
		return planeType;
	}

	private PlaneType findPlaneTypeWithCode(String code) throws MalformedURLException {
		List<PlaneType> allPlaneTypes = getAllPlaneTypes();
		for(PlaneType p : allPlaneTypes) {
			if(p.getCode().equalsIgnoreCase(code)){
				return p;
			}
		}
		log.info("No planeType found for code " + code);
		return null;
	}



	@Override
	public void handlePlaneTypeChangedEvent() {
		log.fine("handlePlaneTypeChangedEvent()");
		log.fine("Plane type cache. Number of elements before removeAll = "+planeTypeCache.getKeys().size());
		log.fine("Clearing all plane Types cached data.");
		planeTypeCache.removeAll();
		log.fine("Plane type cache. Number of elements after removeAll = "+planeTypeCache.getKeys().size());
	}
}
