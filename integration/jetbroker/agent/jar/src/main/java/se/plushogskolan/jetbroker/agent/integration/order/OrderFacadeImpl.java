package se.plushogskolan.jetbroker.agent.integration.order;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

/**
 * Facade implementation to Order artifact.
 * Sends a flight request in a queue using JMS. 
 * "MapMessage" are used for the flight request values.  
 * @author PaulsMacbookPro
 */
@Prod
public class OrderFacadeImpl implements OrderFacade {

	@Inject
	Logger log;

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory factory;
	
	@Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST)
	private Queue requestQueue;

	@Override
	public void sendFlightRequest(FlightRequest request) throws JMSException {
		log.fine("AGENT OrderFacadeImpl. sendFlightRequest() = " + request.toString());
		QueueConnection connection = null;
		QueueSession session = null;
		MapMessage message; 
		QueueSender sender; 
		try {
			connection = factory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			
			message = session.createMapMessage();
			message.setLong("agentRequestId", request.getId());
			message.setString("departureAirportCode", request.getDepartureAirportCode());   // DepartureAirportCode 
			message.setString("arrivalAirportCode", request.getArrivalAirportCode());      // ArrivalAirportCode
			message.setString("departureTime", request.getDepartureTime().toString());    // DepartureTime
			message.setInt("noOfPassengers", request.getNoOfPassengers());               // NoOfPassengers   
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_REQUEST);
			
			sender = session.createSender(requestQueue);
			sender.send(message);
			log.fine("Sending a new FlightRequest to Order with JMS(Queue)");
		} catch (JMSException e) {
			log.severe("Could'nt send new FlightRequest to Order");
			e.printStackTrace();
		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
			log.fine("Closing connection and session");
		}
	}
}
