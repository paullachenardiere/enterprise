package se.plushogskolan.jetbroker.agent.integration.plane;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsAirportArray;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsPlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsPlaneTypeArray;
/**
 * An integration facade to "PLANE". Plane uses JAX-WS (Web Service) to send data.
 * This class uses WSDL to generate classes that's needed.	
 *
 * @Return  List<Airport> , List<PlaneType>
 * @author PaulsMacbookPro
 */
@Prod
public class PlaneFacadeImpl implements PlaneFacade {

	@Inject
	Logger log;

	@Override
	public List<AirPort> getAllAirports() throws MalformedURLException {

		PlaneWebServiceImplService planeWebServiceImplService = new PlaneWebServiceImplService();	
		PlaneWebService planeWebServicePort = planeWebServiceImplService.getPlaneWebServicePort();
		
		WsAirportArray airports = planeWebServicePort.getAirports();
		List<WsAirport> airportsList = airports.getItem();
		List<AirPort> allAirports = new ArrayList<>();
		for(WsAirport a : airportsList) {
			AirPort airport = new AirPort();
			airport.setCode(a.getCode());
			airport.setName(a.getName());
			allAirports.add(airport);
		}
		return allAirports;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {

		PlaneWebServiceImplService planeWebServiceImplService = new PlaneWebServiceImplService();	
		PlaneWebService planeWebServicePort = planeWebServiceImplService.getPlaneWebServicePort();
		
		WsPlaneTypeArray planeTypes = planeWebServicePort.getPlaneTypes();
		List<WsPlaneType> planeTypesList = planeTypes.getItem();
		List<PlaneType> allPlaneTypes = new ArrayList<>();
		for(WsPlaneType p : planeTypesList) {
			PlaneType planeType = new PlaneType();
			planeType.setCode(p.getCode());
			planeType.setFuelConsumptionPerKm(p.getFuelConsumptionPerKm());
			planeType.setMaxNoOfPassengers(p.getMaxNoOfPassengers());
			planeType.setMaxRangeKm(p.getMaxRangeKm());
			planeType.setMaxSpeedKmH(p.getMaxSpeedKmH());
			planeType.setName(p.getName());
			allPlaneTypes.add(planeType);
		}
		return allPlaneTypes;
	}
}
