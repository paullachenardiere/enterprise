package se.plushogskolan.jetbroker.agent.integration.plane;

import java.net.MalformedURLException;
import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;

public interface PlaneFacade {

	public List<AirPort> getAllAirports() throws MalformedURLException;

	public List<PlaneType> getAllPlaneTypes();

}
