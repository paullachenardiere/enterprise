package se.plushogskolan.jetbroker.agent.services;

import java.net.MalformedURLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

/**
 * Service class for the Airport object.
 * Calles the AirPortRepository, PlaneTypeRepository. 
 * @author PaulsMacbookPro
 */

@Stateless
public class AirPortServiceImpl implements AirPortService {

	@Inject
	private AirPortRepository airPortRepository;
	
	@Inject
	private PlaneTypeRepository planeTypeRepository;

	@Override
	public List<AirPort> getAllAirPorts() throws MalformedURLException {
		planeTypeRepository.getAllPlaneTypes();
		return airPortRepository.getAllAirPorts();
	}

	@Override
	public AirPort getAirPort(String code) throws MalformedURLException {
		return airPortRepository.getAirPort(code);
	}

	@Override
	public void handleAirportsChangedEvent() {
		airPortRepository.handleAirportsChangedEvent();
	}
}
