package se.plushogskolan.jetbroker.agent.repository;

import java.net.MalformedURLException;
import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.AirPort;

@Local
public interface AirPortRepository {

	List<AirPort> getAllAirPorts() throws MalformedURLException;

	AirPort getAirPort(String code) throws MalformedURLException;

	void handleAirportsChangedEvent();

}
