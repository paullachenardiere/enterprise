package se.plushogskolan.jetbroker.agent.integration.order.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

/**
 * MDB recives a JMS message from the Order artifact.
 * This mdb handles the response message "FlightRequestConfirmation".   
 * @author PaulsMacbookPro
 */

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
		propertyName="destinationType",
		propertyValue="javax.jms.Queue"),
		@ActivationConfigProperty(
		propertyName="destination",
		propertyValue=JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE),
		@ActivationConfigProperty(
		propertyName="messageSelector",
		propertyValue="messageType = '" + JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_CONFIRMATION + "'")
		})
public class FlightRequestConfirmationMDB extends AbstractMDB implements MessageListener{
	
	@Inject
	Logger log;
	
	@Inject
	FlightRequestService flightRequestService;

	FlightRequestConfirmation flightRequestConfirmation;
	
	@Override
	public void onMessage(Message mess) {
		
		MapMessage message = (MapMessage) mess;
		flightRequestConfirmation = new FlightRequestConfirmation();
		
		try {
			flightRequestConfirmation.setAgentRequestId(Long.valueOf(message.getLong("agentId")));
			flightRequestConfirmation.setOrderRequestId(Long.valueOf(message.getLong("confirmationID")));
			log.fine("Receiving flightRequestConfirmation from Order");
		} catch (JMSException e) {
			log.fine("Failure while receiving flightRequestConfirmation from Order");
			e.printStackTrace();
		}
		flightRequestService.handleFlightRequestConfirmation(flightRequestConfirmation);
	}
}
