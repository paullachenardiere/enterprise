package se.plushogskolan.jetbroker.agent.services;

import java.net.MalformedURLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;
/**
 * Service class for the Plane type object.
 * Calles the PlaneTypeRepository.
 * @author PaulsMacbookPro
 */
@Stateless
public class PlaneServiceImpl implements PlaneService {

	@Inject
	private PlaneTypeRepository planeTypeRepository;
	

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<PlaneType> getAllPlaneTypes() throws MalformedURLException {
		return getPlaneTypeRepository().getAllPlaneTypes();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public PlaneType getPlaneType(String code) throws MalformedURLException {
		return getPlaneTypeRepository().getPlaneType(code);
	}

	public PlaneTypeRepository getPlaneTypeRepository() {
		return planeTypeRepository;
	}

	public void setPlaneTypeRepository(PlaneTypeRepository planeTypeRepository) {
		this.planeTypeRepository = planeTypeRepository;
	}
	
	@Override
	public void handlePlaneTypesChangedEvent() {
		planeTypeRepository.handlePlaneTypeChangedEvent();
	}

}
