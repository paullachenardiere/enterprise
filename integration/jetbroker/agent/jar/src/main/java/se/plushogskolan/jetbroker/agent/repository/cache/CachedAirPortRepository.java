package se.plushogskolan.jetbroker.agent.repository.cache;

import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

/**
 * Takes care of getting all airports from "PLANE". Then saves the data into cache.
 * Contains logic if airports exists in cache or not.   
 * Clears the cache if any changes are made on airports that is saves the "PLANE" artifact.  
 * @author PaulsMacbookPro
 */
@Stateless
public class CachedAirPortRepository implements AirPortRepository {

	@Inject
	Logger log;

	@Prod
	@Inject
	private PlaneFacade planeFacade;

	private static final String All_AIRPORTS = "allAirports";
	private static final String AIRPORT_CACHE = "airportCache";

	// Gets the cache instance.
	private static Cache airportCache = CacheManager.getInstance().getCache(AIRPORT_CACHE);

	@SuppressWarnings("unchecked")
	@Override
	public List<AirPort> getAllAirPorts() throws MalformedURLException {
		log.fine("Get all airports.");

		boolean keyInCache = airportCache.isKeyInCache(All_AIRPORTS);

		if(!keyInCache) {
			log.fine("Airports does not exists in cache");
			log.fine("Airports cache. Number of elements = "+airportCache.getKeys().size());
			List<AirPort> allAirports = planeFacade.getAllAirports();
			log.fine("Saves airports in cache");
			airportCache.put(new Element(All_AIRPORTS, allAirports));
			log.fine("Airports cache. Number of elements = "+airportCache.getKeys().size());
			return allAirports;
		} 
		log.fine("Airports exists in cache!");
		return (List<AirPort>) airportCache.get(All_AIRPORTS).getObjectValue();
	}

	@Override
	public AirPort getAirPort(String code) throws MalformedURLException {
		log.fine("get Airport, code="+ code );

		if(airportCache.isKeyInCache(code)) {
			log.fine("Airport exists in cache with it's own key");
			return (AirPort) airportCache.get(code).getObjectValue();
		} 
		log.fine("Airport doesn't exists in cache with it's own key");
		AirPort airPort = findAirportWithCode(code);
		log.fine("adding airport in cache. Code = "+ airPort.getCode());
		airportCache.put(new Element(airPort.getCode(), airPort));
		return airPort;
	}

	private AirPort findAirportWithCode(String code) throws MalformedURLException {
		List<AirPort> allAirPorts = getAllAirPorts();
		for(AirPort a : allAirPorts) {
			if(a.getCode().equalsIgnoreCase(code)){
				return a;
			}
		}
		log.info("No airport found for code " + code);
		return null;
	}



	@Override
	public void handleAirportsChangedEvent() {
		log.fine("handleAirportsChangedEvent()");
		log.fine("Airports cache. Number of elements before removeAll = "+airportCache.getKeys().size());
		log.fine("Clearing all airports cached data.");
		airportCache.removeAll();
		log.fine("Airports cache. Number of elements after removeAll = "+airportCache.getKeys().size());
	}
}