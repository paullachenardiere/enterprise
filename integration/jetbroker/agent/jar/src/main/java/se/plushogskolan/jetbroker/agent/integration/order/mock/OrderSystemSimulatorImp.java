package se.plushogskolan.jetbroker.agent.integration.order.mock;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Mock;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged.ChangeType;
import se.plushogskolan.jetbroker.agent.events.FlightRequestChangedEvent;

@Stateless
public class OrderSystemSimulatorImp implements OrderSystemSimulator {

	@Inject
	Logger log;

	@Inject
	@FlightRequestChanged(ChangeType.CONFIRMED)
	@Mock
	Event<FlightRequestChangedEvent> flightRequestConfirmationEvent;

	@Inject
	@FlightRequestChanged(ChangeType.OFFER_RECEIVED)
	@Mock
	Event<FlightRequestChangedEvent> flightRequestOfferRecievedEvent;

	@Inject
	@FlightRequestChanged(ChangeType.REJECTED)
	@Mock
	Event<FlightRequestChangedEvent> flightRequestRejectedEvent;

	public void fakeConfirmationFromOrderSystem(long flightRequestId) {
		log.info("fakeConfirmationFromOrderSystem");
		FlightRequestChangedEvent event = new FlightRequestChangedEvent(flightRequestId);
		flightRequestConfirmationEvent.fire(event);

	}

	public void fakeOfferFromOrderSystem(long flightRequestId) {
		log.info("fakeOfferFromOrderSystem");
		FlightRequestChangedEvent event = new FlightRequestChangedEvent(flightRequestId);
		flightRequestOfferRecievedEvent.fire(event);
	}

	public void fakeRejectionFromOrderSystem(long flightRequestId) {
		log.info("fakeRejectionFromOrderSystem");
		FlightRequestChangedEvent event = new FlightRequestChangedEvent(flightRequestId);
		flightRequestRejectedEvent.fire(event);
	}

}
