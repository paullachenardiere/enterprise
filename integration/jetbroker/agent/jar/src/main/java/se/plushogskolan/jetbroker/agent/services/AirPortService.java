package se.plushogskolan.jetbroker.agent.services;

import java.net.MalformedURLException;
import java.util.List;
import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.AirPort;

@Local
public interface AirPortService {

	AirPort getAirPort(String code) throws MalformedURLException;

	List<AirPort> getAllAirPorts() throws MalformedURLException;

	void handleAirportsChangedEvent();

}
