package se.plushogskolan.jetbroker.agent.integration.plane.mdb;


import java.util.logging.Logger;

import javax.ejb.MessageDriven;
import javax.ejb.ActivationConfigProperty;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.services.PlaneService;

/**
* MDB recives a JMS message from the Plane artifact.
* This mdb handles updates on planeTypes.     
* @author PaulsMacbookPro
*/

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
		propertyName="destinationType",
		propertyValue="javax.jms.Topic"),
		@ActivationConfigProperty(
		propertyName="destination",
		propertyValue="java:jboss/exported/jms/topic/planeBroadcastTopic"),
		@ActivationConfigProperty(
		propertyName="messageSelector",
		propertyValue="messageType = '" + JmsConstants.MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED + "'")
		})
public class PlaneTypeChangedMDB extends AbstractMDB implements MessageListener {
	
	@Inject
	Logger log;
	
	@Inject
	PlaneService planeService;
	
	@Override
	public void onMessage(Message message) {
		log.fine("AGENT on message(). MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED. Message = "+ message.toString());
		log.fine("Calling planeService.handlePlaneTypesChangedEvent()");
		planeService.handlePlaneTypesChangedEvent();		
	}
}
