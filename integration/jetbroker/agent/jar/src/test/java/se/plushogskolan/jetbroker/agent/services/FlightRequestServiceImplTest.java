package se.plushogskolan.jetbroker.agent.services;

import junit.framework.Assert;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {

	@Test
	public void createFlightRequest() throws Exception {
		FlightRequestServiceImpl service = new FlightRequestServiceImpl();

		FlightRequestRepository repo = EasyMock.createMock(FlightRequestRepository.class);		
		OrderFacade orderFacade = EasyMock.createMock(OrderFacade.class);
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(TestFixture.getValidCustomer(1));
		orderFacade.sendFlightRequest(flightRequest);

		flightRequest.setId(1);
		EasyMock.expect(repo.persist(flightRequest)).andReturn(1L);
		EasyMock.expect(repo.findById(1)).andReturn(flightRequest);

		EasyMock.replay(repo, orderFacade);
		service.setRepository(repo);
		service.setOrderFacade(orderFacade);

		FlightRequest savedFlightRequest = service.createFlightRequest(flightRequest);

		EasyMock.verify(repo, orderFacade);
		Assert.assertEquals(1, savedFlightRequest.getId());
	}


	@Test
	public void handleFlightRequestConfirmation() throws Exception {
		
		FlightRequestServiceImpl service = new FlightRequestServiceImpl();

		FlightRequest flightRequest = TestFixture.getValidFlightRequest(TestFixture.getValidCustomer(1));
		FlightRequestRepository repo = EasyMock.createMock(FlightRequestRepository.class);
		EasyMock.expect(repo.findById(1)).andReturn(flightRequest);
		
		Capture<FlightRequest> capture = new Capture<FlightRequest>();
		
		repo.update(EasyMock.capture(capture));
		EasyMock.replay(repo);
		
		service.setRepository(repo);
		FlightRequestConfirmation flightRequestConfirmation = new FlightRequestConfirmation(1, 1);
		service.handleFlightRequestConfirmation(flightRequestConfirmation);
		FlightRequest flightRequestCaptured = capture.getValue();
		EasyMock.verify(repo);
		
		Assert.assertEquals("Test status", FlightRequestStatus.REQUEST_CONFIRMED, flightRequestCaptured.getRequestStatus());
		
	}
		
	

}
