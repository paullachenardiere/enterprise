package se.plushogskolan.jetbroker.plane.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;

@Local
public interface PlaneService {

	PlaneType getPlaneType(long id);

	PlaneType createPlaneType(PlaneType planeType) throws Exception;

	void updatePlaneType(PlaneType planeType);

	List<PlaneType> getAllPlaneTypes();

	void deletePlaneType(long id) throws Exception;

}
