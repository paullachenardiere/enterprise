package se.plushogskolan.jetbroker.plane.service;

import javax.ejb.Local;
import javax.ejb.Lock;
import javax.ejb.LockType;

@Local
public interface FuelPriceService {

	@Lock(LockType.READ)
	double getFuelCost();

	@Lock(LockType.WRITE)
	void updateFuelCost(double fuelCost);
}
