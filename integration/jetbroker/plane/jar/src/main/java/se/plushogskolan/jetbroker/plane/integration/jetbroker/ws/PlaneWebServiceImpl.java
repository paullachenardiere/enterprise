package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.repository.AirportRepository;
import se.plushogskolan.jetbroker.plane.repository.PlaneTypeRepository;

@Stateless
@WebService(name = "PlaneWebService",
endpointInterface = "se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.PlaneWebService")
public class PlaneWebServiceImpl implements PlaneWebService {

	@Inject
	Logger log;
	@Inject
	private AirportRepository airportRepository;
	@Inject
	private PlaneTypeRepository planeTypeRepository;
	
	@Override
	public List<WSAirport> getAirports() {
		log.fine("PLANE - PlaneWebServiceImpl. getAirports()");
		List<Airport> allAirports = getAirportRepository().getAllAirports();
		List<WSAirport> allWSAirports = new ArrayList<>();
		for(Airport a : allAirports) {
			WSAirport wsAirport = new WSAirport();
			wsAirport.setCode(a.getCode());
			wsAirport.setLatitude(a.getLatitude());
			wsAirport.setLongitude(a.getLongitude());
			wsAirport.setName(a.getName());
			allWSAirports.add(wsAirport);
		}
		return allWSAirports;
	}

	@Override
	public List<WSPlaneType> getPlaneTypes() {
		log.fine("PLANE - PlaneWebServiceImpl. getPlaneTypes()");
		List<PlaneType> allPlaneTypes = getPlaneTypeRepository().getAllPlaneTypes();
		List<WSPlaneType> allWSPlaneTypes = new ArrayList<>();
		for(PlaneType p : allPlaneTypes) {
			WSPlaneType wsPlaneType = new WSPlaneType();
			wsPlaneType.setCode(p.getCode());
			wsPlaneType.setFuelConsumptionPerKm(p.getFuelConsumptionPerKm());
			wsPlaneType.setMaxNoOfPassengers(p.getMaxNoOfPassengers());
			wsPlaneType.setMaxRangeKm(p.getMaxRangeKm());
			wsPlaneType.setMaxSpeedKmH(p.getMaxSpeedKmH());
			wsPlaneType.setName(p.getName());
			allWSPlaneTypes.add(wsPlaneType);
		}
		return allWSPlaneTypes;
	}

	public AirportRepository getAirportRepository() {
		return airportRepository;
	}

	public void setAirportRepository(AirportRepository airportRepository) {
		this.airportRepository = airportRepository;
	}

	public PlaneTypeRepository getPlaneTypeRepository() {
		return planeTypeRepository;
	}

	public void setPlaneTypeRepository(PlaneTypeRepository planeTypeRepository) {
		this.planeTypeRepository = planeTypeRepository;
	}
}
