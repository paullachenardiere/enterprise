package se.plushogskolan.jetbroker.plane.service;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.plane.repository.AirportRepository;

/**
 * Service for all Airports.
 * Calles the PlaneIntegrationFacade and the AirportRepository 
 * @author PaulsMacbookPro
 */
@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	Logger log;

	@Inject
	private AirportRepository airportRepo;
	
	@Inject
	@Prod
	private PlaneIntegrationFacade planeIntegrationFacade;

	@Override
	public Airport getAirport(long id) {
		log.fine("PLANE AirportServiceImpl. getAirport() id="+id);
		return getAirportRepo().findById(id);
	}

	@Override
	public Airport getAirportByCode(String code) {
		log.fine("PLANE AirportServiceImpl. getAirportByCode() code="+code);
		return getAirportRepo().getAirportByCode(code);
	}

	@Override
	public void updateAirport(Airport airport) {
		log.fine("PLANE AirportServiceImpl. updateAirport() airport = "+airport.toString());
		getAirportRepo().update(airport);
		planeIntegrationFacade.broadcastAirportsChanged();
	}

	@Override
	public Airport createAirport(Airport airport) {
		log.fine("PLANE AirportServiceImpl. createAirport() airport = "+airport.toString());
		long id = getAirportRepo().persist(airport);
		planeIntegrationFacade.broadcastAirportsChanged();
		return getAirport(id);
	}

	@Override
	public List<Airport> getAllAirports() {
		log.fine("PLANE AirportServiceImpl. getAllAirports()");
		List<Airport> airports = getAirportRepo().getAllAirports();
		Collections.sort(airports);
		return airports;
	}

	@Override
	public void deleteAirport(long id) {
		log.fine("PLANE AirportServiceImpl. deleteAirport() id = "+ id);
		Airport airport = getAirport(id);
		getAirportRepo().remove(airport);
		planeIntegrationFacade.broadcastAirportsChanged();
	}

	@Override
	public void updateAirportsFromWebService() {
		// Implement
	}

	public AirportRepository getAirportRepo() {
		return airportRepo;
	}

	public void setAirportRepo(AirportRepository airportRepo) {
		this.airportRepo = airportRepo;
	}

}
