package se.plushogskolan.jetbroker.plane.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.plane.repository.PlaneTypeRepository;

/**
 * Service for all plane types. 
 * Calles the PlaneIntegrationFacade and the PlaneTypeRepository 
 * @author PaulsMacbookPro
 */
@Stateless
public class PlaneServiceImpl implements PlaneService {

	@Inject
	Logger log;
	
	@Inject
	private PlaneTypeRepository planeRepository;
	
	@Inject
	@Prod
	private PlaneIntegrationFacade planeIntegrationFacade;

	@Override
	public PlaneType getPlaneType(long id) {
		log.fine("PLANE PlaneServiceImpl. getPlaneType() id = " + id);
		return getPlaneRepository().findById(id);
	}

	@Override
	public PlaneType createPlaneType(PlaneType planeType) {
		log.fine("PLANE PlaneServiceImpl. createPlaneType() planetype = " + planeType.toString());
		long id = getPlaneRepository().persist(planeType);
		planeIntegrationFacade.broadcastPlaneTypesChanged();
		return getPlaneType(id);
	}

	@Override
	public void updatePlaneType(PlaneType planeType) {
		log.fine("PLANE PlaneServiceImpl. updatePlaneType() planetype = " + planeType.toString());
		getPlaneRepository().update(planeType);
		planeIntegrationFacade.broadcastPlaneTypesChanged();
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("PLANE PlaneServiceImpl. getAllPlaneTypes()");
		return getPlaneRepository().getAllPlaneTypes();
	}

	@Override
	public void deletePlaneType(long id) {
		log.fine("PLANE PlaneServiceImpl. deletePlaneType() id = " + id);
		PlaneType planeType = getPlaneType(id);
		getPlaneRepository().remove(planeType);
		planeIntegrationFacade.broadcastPlaneTypesChanged();
	}

	public PlaneTypeRepository getPlaneRepository() {
		return planeRepository;
	}

	public void setPlaneRepository(PlaneTypeRepository planeRepository) {
		this.planeRepository = planeRepository;
	}

}
