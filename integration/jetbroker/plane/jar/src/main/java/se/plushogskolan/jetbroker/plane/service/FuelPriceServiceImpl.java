package se.plushogskolan.jetbroker.plane.service;

import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;

/**
 * Service for the fuel price with a Singleton bean behavior. 
 * @author PaulsMacbookPro
 */
@Singleton
@Startup
public class FuelPriceServiceImpl implements FuelPriceService {

	private double fuelPrice = 5.2;

	@Inject
	Logger log;
	
	@Inject 
	@Prod
	PlaneIntegrationFacade planeIntegrationFacade;

	@Override
	public double getFuelCost() {
		log.fine("PLANE FuelPriceServiceImpl. getFuelCost()");
		return fuelPrice;
	}

	@Override
	public void updateFuelCost(double price) {
		log.fine("PLANE FuelPriceServiceImpl. updateFuelCost() price = " + price);
		fuelPrice = price;
		planeIntegrationFacade.broadcastNewFuelPrice(price);
	}
}