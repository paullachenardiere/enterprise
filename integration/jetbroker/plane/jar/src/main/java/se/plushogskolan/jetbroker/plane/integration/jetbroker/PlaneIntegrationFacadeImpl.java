package se.plushogskolan.jetbroker.plane.integration.jetbroker;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;

/**
 * Produces and publish a topic message for the new fuel price
 * Broadcast to subscribers if there are any changes in airports or plane types.
 * 
 * @author PaulsMacbookPro
 */
@Prod
public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade{

	@Inject
	Logger log;
	
	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private TopicConnectionFactory connectionFactory;

	@Resource(mappedName = JmsConstants.TOPIC_PLANE_BROADCAST)
	private Topic topic;

	
	@Override
	public void broadcastNewFuelPrice(double fuelPrice) {
		log.fine("PLANE - calling BroadcastNewFuelPrice() = "+ fuelPrice);
		TopicConnection topicConnection = null;
		TopicSession topicSession = null;
		Message message;
		
		try {
			
			topicConnection = connectionFactory.createTopicConnection();
			topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			
			message = topicSession.createMessage();
			message.setDoubleProperty("fuelPrice", fuelPrice);
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED);
			
			TopicPublisher publisher = topicSession.createPublisher(topic);
			publisher.publish(message);
			log.fine("New fuel price. Broadcasting (Topic) to subscribers. Price = "+fuelPrice);
			
		} catch (JMSException e) {
			log.severe("Error in broadcastNewFuelPrice = "+ fuelPrice);
			e.printStackTrace();
		} finally {
			// will exit the connections.
			JmsHelper.closeConnectionAndSession(topicConnection, topicSession);
			log.fine("Closing connection and session");
		}
	}

	@Override
	public void broadcastAirportsChanged() {
		log.fine("PLANE - calling broadcastAirportsChanged()");
		TopicConnection topicConnection = null;
		TopicSession topicSession = null;
		Message message;
		
	try {
			topicConnection = connectionFactory.createTopicConnection();
			topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			topicConnection.start();
			
			message = topicSession.createMessage();
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED);
			
			TopicPublisher publisher = topicSession.createPublisher(topic);
			publisher.publish(message);
			log.fine("AIRPORTSCHANGED. Broadcasting (Topic) to subscribers.");
			
		} catch (JMSException e) {
			log.severe("Exception during broadcast a changed airport!");
			e.printStackTrace();
		} finally {
			// will exit the connections.
			JmsHelper.closeConnectionAndSession(topicConnection, topicSession);
			log.fine("Closing connection and session");
		}
	}

	@Override
	public void broadcastPlaneTypesChanged() {
		log.fine("PLANE - calling broadcastPlaneTypesChanged");
		TopicConnection topicConnection = null;
		TopicSession topicSession = null;
		Message message;
		
	try {
			topicConnection = connectionFactory.createTopicConnection();
			topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			
			message = topicSession.createMessage();
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED);
			
			TopicPublisher publisher = topicSession.createPublisher(topic);
			publisher.publish(message);
			log.fine("AIRPORTSCHANGED. Broadcasting (Topic) to subscribers.");
			
		} catch (JMSException e) {
			log.severe("Exception during broadcast a changed plane type!");
			e.printStackTrace();
		} finally {
			// will exit the connections.
			JmsHelper.closeConnectionAndSession(topicConnection, topicSession);
			log.fine("Closing connection and session");
		}
	
	}

}
