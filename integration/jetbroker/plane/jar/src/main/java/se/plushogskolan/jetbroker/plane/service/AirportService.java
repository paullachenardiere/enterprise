package se.plushogskolan.jetbroker.plane.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.plane.domain.Airport;

@Local
public interface AirportService {

	Airport getAirport(long id);

	Airport getAirportByCode(String code);

	void updateAirport(Airport airport);

	Airport createAirport(Airport airport) throws Exception;

	List<Airport> getAllAirports();

	void updateAirportsFromWebService();

	void deleteAirport(long id) throws Exception;

}
