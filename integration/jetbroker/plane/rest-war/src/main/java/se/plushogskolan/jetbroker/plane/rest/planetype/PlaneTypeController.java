package se.plushogskolan.jetbroker.plane.rest.planetype;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.CreatePlaneTypeRequest;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.CreatePlaneTypeResponse;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.GetPlaneTypeResponse;
import se.plushogskolan.jetbroker.plane.service.PlaneService;

@Controller
public class PlaneTypeController {

	@Inject
	PlaneService planeService;
	
	Logger log = Logger.getLogger(PlaneTypeController.class.getName());
	
	@RequestMapping(value = "/createPlaneType", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CreatePlaneTypeResponse createPlanetype(@RequestBody CreatePlaneTypeRequest request) throws Exception {
		PlaneType planeType = planeService.createPlaneType(request.buildPlaneType());
		return new CreatePlaneTypeResponse(planeType.getId());
	}
	
	@RequestMapping(value = "/getPlaneType/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetPlaneTypeResponse getPlanetype(@PathVariable long id) {
		return new GetPlaneTypeResponse(planeService.getPlaneType(id));
	}

	@RequestMapping(value = "/deletePlaneType/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deletePlaneType(@PathVariable long id) {
		try {
			planeService.deletePlaneType(id);
			return OkOrErrorResponse.getOkResponse();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return OkOrErrorResponse.getErrorResponse("Error deleting planeType with ID: "+ id);

	}

}
